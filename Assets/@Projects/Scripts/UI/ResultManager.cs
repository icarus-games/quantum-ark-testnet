using System.Linq;
using QuantumArk.Blockchain;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace QuantumArk.UI
{
    public class ResultManager : MonoBehaviour
    {
        private void Start()
        {
            LoadMenu();
        }

        private async void LoadMenu()
        {
            LoadingMenu.Open();

            var nftResult = await BlockchainManager.Instance.MintRandomNFT();
            var bcResult = await BlockchainManager.Instance.MintFungibleToken();
            await Blackboard.Instance.FetchLatestData();
            
            var coinAmount = bcResult.error == null ? BlockchainUtility.GetNumberFromBaseNear(bcResult.result).ToString() : "UNDEFINED";

            LoadingMenu.Close();
            ResultMenu.Open("Level Completed! You Got", Blackboard.Instance.ownedItems.Last(), coinAmount, () => SceneManager.LoadScene("MainMenu"));
        }
    }
}