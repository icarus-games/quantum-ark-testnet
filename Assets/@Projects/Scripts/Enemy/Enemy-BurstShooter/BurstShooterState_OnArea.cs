using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public class BurstShooterState_OnArea : EnemyBaseState
	{
		public BurstShooterState_OnArea(BurstShooterFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			ai.SetOnShoot(delegate { view.PlayShoot(); ai.OnShoot(); });
			control.OnEnemyDeath(delegate { fsm.ChangeState(new BurstShooterState_Die((BurstShooterFSM)fsm)); });
			Observable.EveryFixedUpdate().Subscribe(_ => ObserveUpdate()).AddTo(disposable);
		}

		private void ObserveUpdate()
		{
			if (!ai.IsOnShootArea())
			{
				fsm.ChangeState(new BurstShooterState_Idle((BurstShooterFSM)fsm));
			}

			ai.UpdateFaceShoot();
			if (ai.canShoot) ai.TrySingleShoot();


		}
	}
}
