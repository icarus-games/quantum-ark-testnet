using System;
using UnityEngine;

namespace QuantumArk
{
    public class AppInstaller : MonoBehaviourSingleton<AppInstaller>
    {
        [SerializeField] private AppState appState = null;
        [SerializeField] private Blackboard blackboard = null;

        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
            blackboard.Construct(appState);
        }

        private void Start()
        {
            appState.Initialize();
        }
    }
}