using UnityEngine.SceneManagement;

namespace QuantumArk
{
    public class GameState_GameOver : BaseGameState
    {
        public GameState_GameOver(GameStateFSM fsm) : base(fsm)
        {
        }

        public override void OnEnter()
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}