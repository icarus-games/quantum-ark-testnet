using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace QuantumArk.UI
{
    public class InventoryView : MonoBehaviour
    {
        public FuseView fuseView = null;
        public EquipView equipView = null;
        [SerializeField] private TextMeshProUGUI actionButtonText = null;
        [SerializeField] private Image actionButtonSprite = null;
        [SerializeField] private Sprite[] actionButtonSpriteList = null;

        public Button homeButton = null;
        public Button actionButton = null;

        private Subject<string> onItemClicked;
        private Blackboard blackboard;

        public void Initialize(Blackboard blackboard)
        {
            this.blackboard = blackboard;
            
            fuseView.gameObject.SetActive(false);
            equipView.gameObject.SetActive(false);
        }

        public void ChangeViewState(PostGameViewState viewState)
        {
            equipView.gameObject.SetActive(false);
            fuseView.gameObject.SetActive(false);
            
            switch(viewState)
            {
                case PostGameViewState.Equip:
                    actionButtonText.text = "FUSING";
                    equipView.gameObject.SetActive(true);
                    equipView.Initialize(blackboard);
                    break;
                case PostGameViewState.Fuse:
                    actionButtonText.text = "BACK";
                    fuseView.gameObject.SetActive(true);
                    fuseView.Initialize(blackboard);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(viewState), viewState, null);
            }

            actionButtonSprite.sprite = actionButtonSpriteList[(int) viewState];
        }
    }

    public enum PostGameViewState
    {
        Equip,
        Fuse
    }
}