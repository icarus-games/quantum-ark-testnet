# Welcome to Quantum Ark Testnet!

This is the repository for a proof of concept of a blockchain game project, **Quantum Ark**.

**Quantum Ark** is a sidescrolling co-op action shooter game set in a Neo-Futuristic Tokyo
where human began to upload their consciousness and live in the virtual world.

This demo is built for **Metabuild Hackathon**,
running on **NEAR Protocol** and developed using **Unity** engine.

You can find a playable build [here](https://drive.google.com/file/d/1wiocvceTy1FqFWKPnosg_6aFg_aoL9vt/view?usp=sharing)


## Features in this demo

- Fully playable stage with basic mechanics: movement, attack, enemies, etc
- Equip different skins
- Token drop & random NFT Skin drop
- NFT Skin fuse to produce new skin


## How to Play

### Connect Wallet
- Choose **Connect Wallet**
- You will be redirected to a web page
- Follow the instructions
- You will need a [NEAR testnet wallet](https://wallet.testnet.near.org/) to play

### Game Control
-   Aim: Mouse direction
-   Basic attack: left click (hold to trigger basic attack continously)
-   Special attack: right click
-   Switch weapon: Q
-   Move: WASD
-   Jump: Space
-   Dodge: Shift

### Fusing
- Go to **Inventory**
- Choose **Fusing**
- Choose the skin you want to fuse
- Choose **Fuse**
- Both skin used to fuse will be burned and produce a new one