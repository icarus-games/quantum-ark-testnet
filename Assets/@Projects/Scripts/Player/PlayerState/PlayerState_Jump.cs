﻿using System;
using UniRx;

namespace QuantumArk
{
	public class PlayerState_Jump : BasePlayerState
	{
		private const double DELAY_JUMP_CHECK = 0.1;

		public PlayerState_Jump(PlayerFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			input.OnMovePressed().Subscribe(xValue => control.Move(xValue)).AddTo(disposable);
			input.OnShootPressed().Subscribe(_ => Shoot(_)).AddTo(disposable);
			input.OnDashPressed().Subscribe(_ => Dash()).AddTo(disposable);
			input.OnChangeWeaponPressed().Subscribe(_ => control.SwitchWeapon()).AddTo(disposable);
			input.OnSpecialAttackPressed().Subscribe(_ => SpecialAttack()).AddTo(disposable);
			Observable.Timer(TimeSpan.FromSeconds(DELAY_JUMP_CHECK)).Subscribe(_ => ObserveUpdate()).AddTo(disposable);
			control.AimFace.Subscribe(_ => control.ChangeLastDir(_)).AddTo(disposable);
			control.SetOnPlayerDead(ChangeStateToDie);
		}

		private void ObserveUpdate()
		{
			Observable.EveryUpdate().Subscribe(_ => CheckOnGrounded()).AddTo(disposable);
		}

		private void CheckOnGrounded()
		{
			if (control.IsGrounded())
			{
				view.PlayLanding();
				ChangeStateToIdle();
			}
		}

	}
}
