using UnityEngine.SceneManagement;

namespace QuantumArk
{
	public class GameState_Complete : BaseGameState
	{
		public GameState_Complete(GameStateFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			SceneManager.LoadScene("ResultMenu");
		}
	}
}
