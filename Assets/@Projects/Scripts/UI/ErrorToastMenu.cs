using System;
using System.Collections;
using UnityEngine;

namespace QuantumArk.UI
{
    public class ErrorToastMenu : Menu<ErrorToastMenu>
    {
        [SerializeField] private ErrorToastView view = null;
        [SerializeField] private float duration = 3f;
        
        public static void Open(string message)
        {
            Open();
            Instance.SetMessage(message);
        }

        private void SetMessage(string message)
        {               
            view.SetMessage(message, Close);
        }
    }
}