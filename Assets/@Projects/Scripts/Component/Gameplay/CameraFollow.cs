using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace QuantumArk
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField] private Camera targetCamera = null;
        [SerializeField] private CameraFollowTarget[] followTargets = null;
        [SerializeField] private Vector3 offset = Vector3.zero;
        [SerializeField] private float smoothTime = 0.5f;
        [SerializeField] private bool followX = false;
        [SerializeField] private bool followY = false;

        private Vector3 initialPos = Vector3.zero;
        private Vector3 velocity = Vector3.zero;
        private Vector3 targetPosition = Vector3.zero;

        private void OnValidate()
        {
            if (!targetCamera) targetCamera = GetComponent<Camera>();
        }

        private void Awake()
        {
            if (!targetCamera) Debug.LogError("Camera object is empty!");
            if (followTargets == null || followTargets.Length == 0) Debug.LogError("Follow targets is empty!");
        }

        private void Start()
        {
            initialPos = transform.position;
        }

        private void FixedUpdate()
        {
            // generate the weighted position vector
            Vector4[] weightedTargets = new Vector4[followTargets.Length];
            for (int i = 0; i < weightedTargets.Length; i++)
            {
                var pos = followTargets[i].gameObject.position;
                weightedTargets[i] = new Vector4(pos.x, pos.y, pos.z, followTargets[i].weight);
            }
            
            // calculate the new targetPosition, and add the offset
            GetWeightedAABB(weightedTargets, out var bounds, out targetPosition);
            targetPosition += offset;

            // calculate the smooth damped position
            var camPos = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
            
            // if disable follow, set it to initial position
            if (!followX) camPos.x = initialPos.x;
            if (!followY) camPos.y = initialPos.y;

            // assign
            targetCamera.transform.position = camPos;
        }

        private void GetWeightedAABB(Vector4[] points, out Bounds bounds, out Vector3 weightedCenter)
        {
            var min = new Vector4(float.MaxValue, float.MaxValue, float.MaxValue, float.MaxValue);
            var max = new Vector4(float.MinValue, float.MinValue, float.MinValue, float.MinValue);
            var minW = new Vector3(float.MinValue, float.MinValue, float.MinValue);
            var maxW = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            for (var i = 0; i < points.Length; i++)
            {
                TrySetExtent(points[i] - min, points[i], ref min, ref minW);
                TrySetExtent(max - points[i], points[i], ref max, ref maxW);
            }

            bounds = new Bounds();
            bounds.SetMinMax(min, max);
            var centerBounds = bounds.center;
            var sumW = minW + maxW;

            weightedCenter = new Vector3()
            {
                x = Mathf.Abs(sumW.x) < float.Epsilon ? centerBounds.x : Mathf.LerpUnclamped(min.x, max.x, maxW.x / sumW.x),
                y = Mathf.Abs(sumW.y) < float.Epsilon ? centerBounds.y : Mathf.LerpUnclamped(min.y, max.y, maxW.y / sumW.y),
                z = Mathf.Abs(sumW.z) < float.Epsilon ? centerBounds.z : Mathf.LerpUnclamped(min.z, max.z, maxW.z / sumW.z)
            };
        }

        private void TrySetExtent(Vector3 test, Vector4 point, ref Vector4 limit, ref Vector3 limitWeight)
        {
            if (test.x < 0 || (test.x < float.Epsilon && point.w > limitWeight.x))
            {
                limit.x = point.x;
                limitWeight.x = point.w;
            }

            if (test.y < 0 || (test.y < float.Epsilon && point.w > limitWeight.y))
            {
                limit.y = point.y;
                limitWeight.y = point.w;
            }

            if (test.z < 0 || (test.z < float.Epsilon && point.w > limitWeight.z))
            {
                limit.z = point.z;
                limitWeight.z = point.w;
            }
        }
    }

    [Serializable]
    public class CameraFollowTarget
    {
        public Transform gameObject;
        public float weight = 1;
    }
}