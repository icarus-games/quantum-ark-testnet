using UnityEngine;

namespace QuantumArk
{
	public class Jump : MonoBehaviour
	{
		[SerializeField] private float jumpForce = 250f;
		[SerializeField] private Rigidbody2D rb2d = null;

		public void DoJump()
		{
			Vector2 jumpVector = new Vector2(rb2d.velocity.x, jumpForce);
			rb2d.AddForce(jumpVector, ForceMode2D.Impulse);
		}
	}
}