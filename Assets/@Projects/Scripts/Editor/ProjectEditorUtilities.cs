using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace QuantumArk.Editor
{
	public class ProjectEditorUtilities
	{
		[MenuItem("Tools/Utils/Set Active GameObject #A", false, 51)]
		public static void ActivateObj()
		{
			ControlObj(true);
		}

		[MenuItem("Tools/Utils/Set Deactive GameObject #D", false, 52)]
		public static void DeactiveObj()
		{
			ControlObj(false);
		}

		private static void ControlObj(bool value)
		{
			GameObject selection = Selection.activeTransform.gameObject;
			selection.SetActive(value);
			EditorUtility.SetDirty(selection);
		}

		//[MenuItem("Tools/Utils/OpenScene/SplashScreen #1")]
		//public static void OpenSplashScreen()
		//{
		//	OpenScene("SplashScreen");
		//}

		//[MenuItem("Tools/Utils/OpenScene/MainMenu #2")]
		//public static void OpenMainMenu()
		//{
		//	OpenScene("MainMenu");
		//}

		//[MenuItem("Tools/Utils/OpenScene/Gameplay #3")]
		//public static void OpennGameplay()
		//{
		//	OpenScene("Gameplay");
		//}

		private static void OpenScene(string sceneName)
		{
			EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
			EditorSceneManager.OpenScene(string.Format(@"Assets\@Project\Scenes\{0}.unity", sceneName));
		}
	}
}
