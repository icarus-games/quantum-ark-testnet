using QuantumArk;
using UnityEngine;

namespace QuantumArk.UI
{
    public class GameHudMenu : MonoBehaviour
    {
        [SerializeField] private GameHudView view = null;
        
        private PlayerControl playerControl;
        
        private void Awake()
        {
            ConstructDependencies();
        }

        private void ConstructDependencies()
        {
            playerControl = FindObjectOfType<PlayerControl>();
        }

        private void Start()
        {
            view.Initialize(playerControl);
        }
    }
}