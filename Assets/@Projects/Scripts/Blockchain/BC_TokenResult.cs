using System;

namespace QuantumArk.Blockchain
{
    [Serializable]
    public class BC_TokenResult
    {
        public string _id;
        public string contract_id;
        public string token_id;
        public string owner_id;
        public string token_series_id;
        public BC_TokenResultMetadata metadata;
    }
    
    [Serializable]
    public class BC_TokenResultMetadata
    {
        public string title;
        public string description;
        public string media;
        public string creator_id;
        public string issued_at;
    }
}