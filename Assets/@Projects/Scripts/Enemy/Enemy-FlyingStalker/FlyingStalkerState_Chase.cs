using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public class FlyingStalkerState_Chase : EnemyBaseState
	{
		public FlyingStalkerState_Chase(FlyingStalkerFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			view.PlayAttack();
			view.PlayStalk();
			control.OnEnemyDeath(delegate { fsm.ChangeState(new FlyingStalkerState_Die((FlyingStalkerFSM)fsm)); });
			Observable.EveryFixedUpdate().Subscribe(_ => ObserveUpdate()).AddTo(disposable);
		}

		private void ObserveUpdate()
		{
			ai.UpdateFaceTowardsPlayer();
			ai.ChasePlayer();
		}
	}
}
