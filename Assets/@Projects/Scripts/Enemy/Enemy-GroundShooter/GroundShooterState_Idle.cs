using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public class GroundShooterState_Idle : EnemyBaseState
	{
		public GroundShooterState_Idle(GroundShooterFSM fsm) : base(fsm)
		{
		}


		public override void OnEnter()
		{
			ai.SpawnSingleWeapon();
			view.PlayIdle();
			control.OnEnemyDeath(delegate { fsm.ChangeState(new GroundShooterState_Die((GroundShooterFSM)fsm)); });
			Observable.EveryUpdate().Subscribe(_ => ObserveUpdate()).AddTo(disposable);
		}

		private void ObserveUpdate()
		{
			if (ai.IsOnShootArea())
			{
				fsm.ChangeState(new GroundShooterState_OnArea((GroundShooterFSM)fsm));
			}
		}
	}
}
