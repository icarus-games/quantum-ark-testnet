﻿using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public class PlayerState_Move : BasePlayerState
	{
		private float dir;

		public PlayerState_Move(PlayerFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			input.OnMovePressed().Subscribe(xValue => Move(xValue)).AddTo(disposable);
			input.OnJumpPressed().Subscribe(_ => Jump()).AddTo(disposable);
			input.OnDashPressed().Subscribe(_ => Dash()).AddTo(disposable);
			input.OnShootPressed().Subscribe(_ => Shoot(_)).AddTo(disposable);
			input.OnChangeWeaponPressed().Subscribe(_ => control.SwitchWeapon()).AddTo(disposable);
			input.OnSpecialAttackPressed().Subscribe(_ => SpecialAttack()).AddTo(disposable);
			control.SetOnPlayerDead(ChangeStateToDie);
			view.PlayRun();
			view.SetIsMoving(true);
		}

		public override void OnExit()
		{
			base.OnExit();
			view.SetIsMoving(false);
		}

		private void Move(float dir)
		{
			control.Move(dir);
			view.UpdateRunDirection(dir);
			if (Mathf.Abs(dir) == 0f)
				ChangeStateToIdle();
		}
	}
}
