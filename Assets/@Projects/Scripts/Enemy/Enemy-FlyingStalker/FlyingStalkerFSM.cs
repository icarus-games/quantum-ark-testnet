using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class FlyingStalkerFSM : EnemyFSM
	{
		public override void Init()
		{
			view.PlayInitate();
			ChangeState(new FlyingStalkerState_Idle(this));
		}
	}
}
