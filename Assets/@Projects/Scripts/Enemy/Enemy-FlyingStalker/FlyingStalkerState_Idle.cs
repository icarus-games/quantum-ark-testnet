using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class FlyingStalkerState_Idle : EnemyBaseState
	{
		public FlyingStalkerState_Idle(FlyingStalkerFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			view.PlayIdle();
			control.OnEnemyDeath(delegate { fsm.ChangeState(new FlyingStalkerState_Die((FlyingStalkerFSM)fsm)); });
			fsm.ChangeState(new FlyingStalkerState_Chase((FlyingStalkerFSM)fsm));
		}
	}
}
