using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public class PlayerState_Ultimate : BasePlayerState
	{
		public PlayerState_Ultimate(PlayerFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			control.ActivateSpecialAttack();
			control.ResetVelocity();
			view.PlayUltimate();
			Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe(_ => CheckOnGrounded()).AddTo(disposable);
		}

		private void CheckOnGrounded()
		{
			if (control.IsGrounded())
			{
				view.PlayLanding();
				ChangeStateToIdle();
			}
			else
			{
				fsm.ChangeState(new PlayerState_Jump(fsm));
				view.PlayOnAir();
			}
		}
	}
}
