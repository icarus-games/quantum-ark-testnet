using UnityEngine;
using System.Collections;

public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour 
{
	private static T instance; 

	public static T Instance
    {
		get
        {
            if (!instance) instance = FindObjectOfType<T>();
            if (!instance) instance = new GameObject(typeof(T).Name).AddComponent<T>();

			return instance; 
		}
	}

	protected virtual void Awake()
	{
		if (instance == null) {
			instance = GetComponent<T>();
		}

		DestroyDuplicates();
	}

	void OnDestroy()
	{
		T comp = GetComponent<T>();

		if (comp != null && instance == comp) {
			instance = null;
		}
	}

	void DestroyDuplicates()
	{
		T comp = GetComponent<T>();

		if (comp != null && instance != comp)
		{
			Destroy(gameObject);
		}
	}
}
