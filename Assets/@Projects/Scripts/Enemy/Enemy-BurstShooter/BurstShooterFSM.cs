using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class BurstShooterFSM : EnemyFSM
	{
		public override void Init()
		{
			view.PlayInitate();
			ChangeState(new BurstShooterState_Idle(this));
		}
	}
}
