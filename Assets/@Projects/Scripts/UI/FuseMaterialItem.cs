using UnityEngine;
using UnityEngine.UI;

namespace QuantumArk.UI
{
    public class FuseMaterialItem : MonoBehaviour
    {
        public Button button;
        [SerializeField] private Image image = null;
        [SerializeField] private Sprite emptySprite = null;

        private CosmeticItemView itemView;

        public CosmeticItemView ItemView
        {
            get => itemView;
            set
            {
                itemView = value;
                image.sprite = this.itemView.ImageSprite;
            }
        }
        
        public void ReleaseSelection()
        {
            image.sprite = emptySprite;
            this.itemView = null;
        }

        public bool HasCosmeticItem => itemView != null;

        public CosmeticItem CosmeticItem => itemView != null ? itemView.cosmeticItem : null;
    }
}