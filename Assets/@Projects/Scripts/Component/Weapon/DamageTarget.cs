using System;
using System.Collections.Generic;
using Lean.Pool;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
    public class DamageTarget : MonoBehaviour
    {
        public float maxHealth = 0;
		[SerializeField] private bool hasCustomOnHealthZero = false;
		[SerializeField] private DamageTargetType collisionType = DamageTargetType.OnEnter;
		[SerializeField] private List<Allegiance> canBeDamagedList = null;
		[SerializeField] private DamageTargetView view = null;
        
        [Header("Modified by Script")]
        public FloatReactiveProperty health;

		public Action OnDamaged;
		public Action OnHealthZero;

		private Vector2 otherPos;
		public Vector2 OtherPos => otherPos;

        private void OnEnable()
        {
            health.Value = maxHealth;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
			if (collisionType != DamageTargetType.OnEnter) return;
            var dmgSource = other.gameObject.GetComponent<DamageSource>();
			otherPos = other.transform.position;

			if (dmgSource != null)
            {
                if (CanByDamagedBy(dmgSource))
                {
                    Damage(dmgSource.damagePts);
                }
            }
        }

		private void OnTriggerStay2D(Collider2D other)
		{
			if (collisionType != DamageTargetType.OnStay) return;
			var dmgSource = other.gameObject.GetComponent<DamageSource>();
			otherPos = other.transform.position;

			if (dmgSource != null)
			{
				if (CanByDamagedBy(dmgSource))
				{
					Damage(dmgSource.damagePts);
				}
			}
		}

		private void OnTriggerExit2D(Collider2D other)
		{
			if (collisionType != DamageTargetType.OnExit) return;
			var dmgSource = other.gameObject.GetComponent<DamageSource>();
			otherPos = other.transform.position;

			if (dmgSource != null)
			{
				if (CanByDamagedBy(dmgSource))
				{
					Damage(dmgSource.damagePts);
				}
			}
		}

		public bool CanByDamagedBy(DamageSource source)
        {
            return canBeDamagedList.Contains(source.allegiance);
        }

        public void Damage(float amount)
        {
            health.Value = Mathf.Clamp(health.Value - amount, 0, float.MaxValue);
			OnDamaged?.Invoke();
			if(view != null) view.PlayOnDamaged();

            if (health.Value <= 0f)
            {
				if(hasCustomOnHealthZero) OnHealthZero?.Invoke();
                else LeanPool.Despawn(gameObject);
            }
        }
    }

	public enum DamageTargetType
	{
		OnEnter, OnStay, OnExit
	}
}