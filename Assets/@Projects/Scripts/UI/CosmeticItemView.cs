using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace QuantumArk.UI
{
    public class CosmeticItemView : MonoBehaviour
    {
        public Button actionButton = null;
        public TextMeshProUGUI actionButtonText = null;

        [SerializeField] private TextMeshProUGUI itemName = null;
        [SerializeField] private Image image = null;
        [SerializeField] private Image bg = null;
        
        private Color newItemBgColor = new Color(255f / 255f, 192f / 255f, 46f / 255f);
        private Color defaultBgColor;

        public CosmeticItem cosmeticItem;
        
        public void Initialize(CosmeticItem cosmeticItem)
        {
            this.cosmeticItem = cosmeticItem;
            this.defaultBgColor = bg.color;
            
            image.sprite = Resources.Load<Sprite>($"CosmeticItem/{cosmeticItem.AssetId}");
            itemName.text = cosmeticItem.FullItemName;
        }

        public void FlagNewItem()
        {
            bg.color = newItemBgColor;
        }

        public Sprite ImageSprite => image.sprite;
    }
}