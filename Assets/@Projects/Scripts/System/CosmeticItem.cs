using System;
using QuantumArk.Blockchain;
using UnityEngine;

namespace QuantumArk
{
    [Serializable]
    public class CosmeticItem
    {
        public string nftId;
        public string tokenId;
        public string seriesId;
        public string AssetId => $"jacket_{tokenId}";

        [SerializeField] private CosmeticItemConfig config = null;
        public string ItemName => config.itemName;
        public string FullItemName => $"{ItemName} #{seriesId}";
        
        public CosmeticItem(string nftId)
        {
            this.nftId = nftId;

            var split = nftId.Split(':');
            this.tokenId = split[0];
            this.seriesId = split[1];

            config = Resources.Load<CosmeticItemConfig>($"CosmeticItemConfig/{AssetId}");
        }

        public static CosmeticItem MockItem()
        {
            return new CosmeticItem("1:mock");
        }
    }
}