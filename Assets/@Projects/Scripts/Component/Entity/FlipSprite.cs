﻿using UnityEngine;

namespace QuantumArk
{
	public class FlipSprite : MonoBehaviour
	{
		[SerializeField] private Transform trans = null;

		[SerializeField] private bool isFacingRight = true;

		public bool IsFacingRight => isFacingRight;
		public void Flip(float moveVal)
		{
			if (moveVal > 0 && !isFacingRight)
			{
				Flip();
			}

			if (moveVal < 0 && isFacingRight)
			{
				Flip();
			}
		}

		private void Flip()
		{
			isFacingRight = !isFacingRight;
			//trans.Rotate(0f, 180f, 0f);
			var xScale = isFacingRight ? 1 : -1;
			trans.localScale = new Vector3(xScale, 1f, 1f);
		}
	}
}