using Lean.Pool;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace QuantumArk
{
	public class VFXPlayer : MonoBehaviourSingleton<VFXPlayer>
	{
		public VFXItem[] vfxLists;

		public GameObject SpawnVFX(string vfxName, Vector2 worldPosition, Transform parent, float zRot = 0f)
		{
			VFXItem vfxItem = vfxLists.FirstOrDefault(i => i.vfxName.Equals(vfxName));
			if (vfxItem == null)
			{
				Debug.Log($"VFX with {vfxName} is not found.");
				return null;
			}
			var vfx = vfxItem.vfx;
			var prefab = LeanPool.Spawn(vfx, worldPosition, vfx.transform.rotation);
			prefab.transform.parent = parent;
			prefab.transform.Rotate(0f, 0f, zRot);
			var particle = prefab.GetComponent<ParticleSystem>();
			particle.Play();
			return prefab;
		}

		public GameObject SpawnVFX(string vfxName, Vector2 worldPosition)
		{
			return SpawnVFX(vfxName, worldPosition, transform);
		}

		public void SpawnVFX(string vfxName, Transform parent)
		{
			VFXItem vfxItem = vfxLists.FirstOrDefault(i => i.vfxName.Equals(vfxName));
			if (vfxItem == null)
			{
				Debug.Log($"VFX with {vfxName} is not found.");
				return;
			}
			var vfx = vfxItem.vfx;
			var prefab = LeanPool.Spawn(vfx, parent);
			var particle = prefab.GetComponent<ParticleSystem>();
			particle.Play();
		}
	}

	[System.Serializable]
	public class VFXItem
	{
		public string vfxName;
		public GameObject vfx;
	}
}
