using System;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
    public class AppState : MonoBehaviourSingleton<AppState>
    {
        private const string KEY_EquippedItem = "EquippedItem";

        public StringReactiveProperty equippedItem;

        public void Initialize()
        {
            equippedItem.Value = LoadValue<string>(KEY_EquippedItem);
            
            equippedItem.TakeUntilDestroy(this).Subscribe(value => OnValueChanged(KEY_EquippedItem, value));
        }

        private T LoadValue<T>(string key)
        {
            if (!PlayerPrefs.HasKey(key)) return default(T);
            
            if (typeof(T) == typeof(string))
            {
                return (T)(object)PlayerPrefs.GetString(key);
            }

            return default(T);
        }

        private void OnValueChanged<T>(string key, T value) 
        {
            if (typeof(T) == typeof(string))
            {
                PlayerPrefs.SetString(key, (string)(object)value);
            }
            
            PlayerPrefs.Save();
        }
    }
}