using System;
using TMPro;
using UnityEngine;

namespace QuantumArk.UI
{
    public class ErrorToastView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI errorText = null;

        private Action onComplete;
        public void SetMessage(string message, Action completeCallback)
        {
            errorText.text = $"Error:\n{message}";
            onComplete = completeCallback;
        }

        public void Complete()
        {
            onComplete?.Invoke();
        }
        
    }
}