using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class BulletRecycle : MonoBehaviour
	{
		[SerializeField] private TrailRenderer trail;
		[SerializeField] private string vfxNameOnRecycle;

		[SerializeField] private bool hasCamShakeOnHit = false;
		[SerializeField] CameraShakePreset camShakePreset = CameraShakePreset.Mild;

		private float time;

		private void Awake()
		{
			time = trail.time;
		}

		private void OnEnable()
		{
			StartCoroutine(EnableTrail());
		}

		private void OnDisable()
		{
			if(!string.IsNullOrEmpty(vfxNameOnRecycle)) VFXPlayer.Instance.SpawnVFX(vfxNameOnRecycle, transform.position);
			if(hasCamShakeOnHit) CameraFx.Instance.Shake(camShakePreset);
			trail.time = 0;
		}

		private IEnumerator EnableTrail()
		{
			yield return new WaitForSeconds(0.05f);
			trail.time = this.time;
		}

	}
}
