using System;
using System.Collections.Generic;
using UnityEngine;
using System.Dynamic;
using System.Numerics;
using Newtonsoft.Json;
using NearClient.Utilities;
using MetaseedUnityToolkit;
using System.Threading.Tasks;

namespace QuantumArk.Blockchain
{
    public class BlockchainManager : MonoBehaviourSingleton<BlockchainManager>
    {
        [SerializeField] private PlayerConnector playerConnector = null;
        [SerializeField] private ContractCaller contractCaller = null;

        public string AccountId => playerConnector.GetPlayerAccountId();

        private const string NFT_CONTRACT_ID = "dev-1643368771086-83655298164012";
        private const string FT_CONTRACT_ID = "dev-1642749960038-75584185908370";
        private const string FUSING_PRICE_BASENEAR = "100000000000000000000";
        private const string TOKEN_SERIES_ID_A = "1";
        private const double GAS_AMOUNT = 300; 

        public bool IsPlayerConnected => playerConnector.IsPlayerConnected();
        public double FusingPrice => BlockchainUtility.GetNumberFromBaseNear(FUSING_PRICE_BASENEAR);// double.Parse(fusingPrice) / Math.Pow(10, 18);

        public static event Action<string> OnCallException;

        public async Task ConnectPlayer()
        {
            if (!playerConnector.IsPlayerConnected())
            {
                await playerConnector.ConnectWalletByBrowserAsync();
                await MintTokenAIfNotExist();
            }
            else
            {
                playerConnector.DisconnectWallet();
            }
        }

        public async Task<BlockchainCallResult<string>> MintNFT(string tokenSeriesId)
        {
            try
            {                
                if (!playerConnector.IsPlayerConnected()) return new BlockchainCallResult<string>(string.Empty, new Exception("Player is not connected"));

                Debug.Log(AccountId + " is minting an NFT");

                dynamic arguments = new ExpandoObject();
                arguments.token_series_id = tokenSeriesId;
                arguments.receiver_id = AccountId;

                ulong nearGas = (ulong) UnitConverter.GetGasFormat(GAS_AMOUNT);
                UInt128 yoctoNearDeposit = (UInt128) UnitConverter.GetYoctoNearFormat(0);

                Debug.Log("yoctoNearDeposit: " + yoctoNearDeposit);

                dynamic result = await contractCaller.CallContract(NFT_CONTRACT_ID, "nft_buy", arguments,
                    EConnectionActor.Player, nearGas, yoctoNearDeposit);
                Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));
                return new BlockchainCallResult<string>(JsonConvert.SerializeObject(result), null);
            }
            catch (Exception error)
            {
                Debug.Log(error.Message);
                OnCallException?.Invoke(error.Message);
                return new BlockchainCallResult<string>(string.Empty, error);
            }
        }

        public async Task<BlockchainCallResult<string>> GetAccountBalance()
        {
            try
            {                
                if (!playerConnector.IsPlayerConnected()) return new BlockchainCallResult<string>(string.Empty, new Exception("Player is not connected"));

                Debug.Log("Viewing FT balance");
                dynamic arguments = new ExpandoObject();
                arguments.account_id = AccountId;
                dynamic result =
                    await contractCaller.ViewContract(FT_CONTRACT_ID, "ft_balance_of", arguments, EConnectionActor.Player);
                Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));

                BigInteger balance = BigInteger.Parse((string) result.result);
                BigInteger decimals = BigInteger.Parse("1000000000000000000"); // 10**18

                return new BlockchainCallResult<string>($"{balance / decimals}", null);
            }
            catch (Exception error)
            {
                Debug.Log(error.Message);
                OnCallException?.Invoke(error.Message);
                return new BlockchainCallResult<string>(string.Empty, error);
            }
        }

        public async Task<BlockchainCallResult<string>> MintRandomNFT()
        {
            try
            {                
                if (!playerConnector.IsPlayerConnected()) return new BlockchainCallResult<string>(string.Empty, new Exception("Player is not connected"));

                Debug.Log(AccountId + " is minting an NFT");

                dynamic arguments = new ExpandoObject();
                ulong nearGas = (ulong) UnitConverter.GetGasFormat(GAS_AMOUNT);
                UInt128 yoctoNearDeposit = (UInt128) UnitConverter.GetYoctoNearFormat(0);

                Debug.Log("yoctoNearDeposit: " + yoctoNearDeposit);

                dynamic result = await contractCaller.CallContract(NFT_CONTRACT_ID, "nft_random_loot", arguments,
                    EConnectionActor.Player, nearGas, yoctoNearDeposit);
                Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));
                return new BlockchainCallResult<string>(JsonConvert.SerializeObject(result), null);
            }
            catch (Exception error)
            {
                Debug.Log(error.Message);
                OnCallException?.Invoke(error.Message);
                return new BlockchainCallResult<string>(string.Empty, error);
            }
        }

        public async Task<BlockchainCallResult<string>> MintTokenAIfNotExist()
        {
            try
            {                
                if (!playerConnector.IsPlayerConnected()) return new BlockchainCallResult<string>(string.Empty, new Exception("Player is not connected"));

                dynamic arguments = new ExpandoObject();
                arguments.account_id = AccountId;
                dynamic result = await contractCaller.ViewContract(NFT_CONTRACT_ID, "nft_supply_for_owner", arguments,
                    EConnectionActor.Player);
                Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));

                int totalNftforOwner = int.Parse(result.result);
                Debug.Log(totalNftforOwner);
                if (totalNftforOwner > 0)
                {
                    // Mint token A
                    Debug.Log("Checking if user has token A");
                    for (int i = 0; i < totalNftforOwner / 10 + 1; i++)
                    {
                        dynamic argumentsI = new ExpandoObject();
                        argumentsI.account_id = AccountId;
                        argumentsI.start_index = i * 10;
                        argumentsI.limit = (i + 1) * 10;
                        dynamic resultI = await contractCaller.ViewContract(NFT_CONTRACT_ID, "nft_tokens_for_owner",
                            arguments, EConnectionActor.Player);
                        Debug.Log(JsonConvert.SerializeObject(resultI));
                        dynamic resultJson = JsonConvert.DeserializeObject(resultI.result);
                        Debug.Log(resultJson[0].token_id);
                        for (int j = 0; j < resultJson.Count; j++)
                        {
                            string tokenSeriesId = ((string) resultJson[j].token_id).Split(':')[0];
                            if (tokenSeriesId == TOKEN_SERIES_ID_A)
                            {
                                Debug.Log("Minted token:" + " You already have token A");
                                return new BlockchainCallResult<string>("", null);
                            }
                        }
                    }
                }

                return await MintNFT(TOKEN_SERIES_ID_A);
            }
            catch (Exception error)
            {
                Debug.Log(error.Message);
                OnCallException?.Invoke(error.Message);
                return new BlockchainCallResult<string>(string.Empty, error);
            }
        }

        public async Task<BlockchainCallResult<string>> MintFungibleToken()
        {
            try
            {
                if (!playerConnector.IsPlayerConnected()) return new BlockchainCallResult<string>(string.Empty, new Exception("Player is not connected"));

                Debug.Log(AccountId + " is minting an FT");

                dynamic arguments = new ExpandoObject();
                arguments.receiver_id = AccountId;

                ulong nearGas = (ulong) UnitConverter.GetGasFormat(GAS_AMOUNT);
                UInt128 yoctoNearDeposit = (UInt128) UnitConverter.GetYoctoNearFormat(0);

                Debug.Log("yoctoNearDeposit: " + yoctoNearDeposit);

                dynamic result = await contractCaller.CallContract(FT_CONTRACT_ID, "mint_tokens", arguments,
                    EConnectionActor.Player, nearGas, yoctoNearDeposit);
                Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));

                return new BlockchainCallResult<string>(JsonConvert.SerializeObject(result), null);
            }
            catch (Exception error)
            {
                Debug.Log(error.Message);
                OnCallException?.Invoke(error.Message);
                return new BlockchainCallResult<string>(string.Empty, error);
            }
            
        }

        public async Task<BlockchainCallResult<string>> Fuse(string tokenA, string tokenB, string targetTokenSeriesId)
        {
            try
            {
                if (!playerConnector.IsPlayerConnected()) return new BlockchainCallResult<string>("Error: Player is not connected!", null);

                Debug.Log(AccountId + " is fusing an NFT");

                dynamic arguments = new ExpandoObject();
                arguments.receiver_id = NFT_CONTRACT_ID;
                arguments.amount = FUSING_PRICE_BASENEAR;
                arguments.msg = // recode this using Json
                    "{\"token_ids\":[\"" +
                    tokenA +
                    "\",\"" +
                    tokenB +
                    "\"]," +
                    "\"target_token_series_id\":" +
                    "\"" +
                    targetTokenSeriesId +
                    "\"}";
                ulong nearGas = (ulong) UnitConverter.GetGasFormat(GAS_AMOUNT);
                UInt128 yoctoNearDeposit = (UInt128) UnitConverter.GetYoctoNearFormat(0);

                Debug.Log("yoctoNearDeposit: " + yoctoNearDeposit);

                dynamic result = await contractCaller.CallContract(FT_CONTRACT_ID, "ft_transfer_call_unsafe", arguments,
                    EConnectionActor.Player, nearGas, yoctoNearDeposit);
                Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));

                dynamic arguments2 = new ExpandoObject();
                arguments2.account_id = AccountId;
                dynamic result2 = await contractCaller.ViewContract(NFT_CONTRACT_ID, "get_last_token_by_owner", arguments2,
                    EConnectionActor.Player);
                Debug.Log("Blockchain has returned the result of contract calling: " +
                          JsonConvert.SerializeObject(result2));

                return new BlockchainCallResult<string>(JsonConvert.SerializeObject(result2.result), null);
            }
            catch (Exception error)
            {
                Debug.Log(error.Message);
                OnCallException?.Invoke(error.Message);
                return new BlockchainCallResult<string>(string.Empty, error);
            }
        }

        public async Task<BlockchainCallResult<List<string>>> GetOwnedNFT()
        {
            try
            {
                
                if (!playerConnector.IsPlayerConnected()) return new BlockchainCallResult<List<string>>(null, new Exception("Player is not connected"));

                dynamic arguments = new ExpandoObject();
                arguments.account_id = AccountId;
                dynamic result = await contractCaller.ViewContract(NFT_CONTRACT_ID, "nft_supply_for_owner", arguments,
                    EConnectionActor.Player);
                Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));

                int totalNftforOwner = int.Parse(result.result);
                List<string> resultItems = new List<string>();
                Debug.Log(totalNftforOwner);
                if (totalNftforOwner > 0)
                {
                    Debug.Log("Checking if user has token A");
                    for (int i = 0; i < totalNftforOwner / 10 + 1; i++)
                    {
                        dynamic argumentsI = new ExpandoObject();
                        argumentsI.account_id = AccountId;
                        argumentsI.start_index = i * 10;
                        argumentsI.limit = (i + 1) * 10;
                        dynamic resultI = await contractCaller.ViewContract(NFT_CONTRACT_ID, "nft_tokens_for_owner",
                            arguments, EConnectionActor.Player);
                        Debug.Log(JsonConvert.SerializeObject(resultI));
                        dynamic resultJson = JsonConvert.DeserializeObject(resultI.result);
                        for (int j = 0; j < resultJson.Count; j++)
                        {
                            resultItems.Add((string) resultJson[j].token_id);
                        }
                    }
                }

                return new BlockchainCallResult<List<string>>(resultItems, null);
            }
            catch (Exception error)
            {
                Debug.Log(error.Message);
                OnCallException?.Invoke(error.Message);
                return new BlockchainCallResult<List<string>>(null, error);
            }
        }
    }
}