using System;
using UnityEngine;
using UniRx;
using System.Collections;

namespace QuantumArk
{
	public class PlayerControl : MonoBehaviour
	{
		private PlayerConfig config;
		[SerializeField] private PlayerView view;

		[SerializeField] private Movement movement;
		[SerializeField] private Jump jump;
		[SerializeField] private GroundCheck groundCheck;
		[SerializeField] private PlayerWeapon weapon;
		[SerializeField] private MuzzleAimCrosshair aimCrosshair;
		[SerializeField] private PlayerSpecialAttack specialAttack = null;
		[SerializeField] private DamageTarget damageTarget;
		[SerializeField] private CapsuleCollider2D damageCollider;

		public BoolReactiveProperty IsShooting = new BoolReactiveProperty(false);

		private bool isDashCooldown = false;
		private bool isSwitchWeaponCooldown = false;
		private Coroutine co;

		private void Start()
		{
			IsShooting.TakeUntilDisable(this).Subscribe(_ => view.PlayShoot(_));
		}

		private void OnEnable()
		{
			damageTarget.OnDamaged = delegate 
			{
				DisableBodyTrigger(config.invulnerableOnDamagedDuration);
				view.PlayDamaged(damageTarget.OtherPos);
			};
			
			specialAttack.BulletSpawner.OnBulletSpawned = delegate
			{
				AudioPlayer.Instance.PlaySfx("rocketFalling");
			};
		}

		public void DisableBodyTrigger(float dur)
		{
			if(co != null)
			{
				StopCoroutine(co);
				co = null;
			}
			co = StartCoroutine(DisableBodyTriggerCo(dur));
		}

		private IEnumerator DisableBodyTriggerCo(float dur)
		{
			damageCollider.enabled = false;
			yield return new WaitForSeconds(dur);
			damageCollider.enabled = true;

		}

		public void SetPlayerConfig(PlayerConfig config) => this.config = config;

		public void SetOnPlayerDead(Action act) => damageTarget.OnHealthZero = act;

		public void Jump() => jump.DoJump();

		public void Move(float dir) => movement.Move(dir);

		public void ResetVelocity() => movement.ResetVelocity();

		public void ChangeLastDir(int faceDir) => movement.ChangeLastDir(faceDir);

		public bool IsGrounded() => groundCheck.CheckGrounded();

		public float GetXVelocity() => movement.CurrentVelocity().x;

		public void TryShoot() => weapon.ActiveWeapon.TryShoot();

		public void ActivateSpecialAttack() => specialAttack.ActivateSkill();

		public bool IsUltimateReady() => specialAttack.IsReady;

		public IntReactiveProperty ActiveWeaponIndex => weapon.activeWeaponIndex;
		
		public FloatReactiveProperty Health => damageTarget.health;
		public float MaxHealth => damageTarget.maxHealth;
		public FloatReactiveProperty SpecialAttackTime => specialAttack.time;
		public float SpecialAttackProgress => specialAttack.Progress;

		public void Dash()
		{
			movement.Dash();
		}

		public void StartDashCooldown()
		{
			StartCoroutine(CooldownCo(config.DashCooldown, _ => isDashCooldown = _));
		}

		public void SwitchWeapon()
		{
			if (isSwitchWeaponCooldown) return;
			isSwitchWeaponCooldown = true;
			weapon.SwitchWeapon();
			AudioPlayer.Instance.PlaySfx("weaponSwitch");
			view.SetWeaponSkin(weapon.ActiveWeapon.weaponName);
			StartCoroutine(CooldownCo(config.SwitchWeaponCooldown, _ => isSwitchWeaponCooldown = _));
		}

		public IntReactiveProperty AimFace => aimCrosshair.facingSign;

		public bool IsDashCooldown => isDashCooldown;

		public bool SetIsDashCooldown(bool value) => isDashCooldown = value;

		public bool IsSwitchWeaponCooldown => isSwitchWeaponCooldown;

		private IEnumerator CooldownCo(float cd, Action<bool> onEnds)
		{
			yield return new WaitForSeconds(cd);
			onEnds?.Invoke(false);
		}
	}
}