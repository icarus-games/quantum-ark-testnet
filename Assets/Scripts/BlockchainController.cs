using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Dynamic;
using System.Numerics;
using UnityEngine.UI;
using Newtonsoft.Json;
using NearClient.Utilities;
using MetaseedUnityToolkit;
using UnityEngine.Networking;
using UnityEngine.Events;
using System;

public class BlockchainController : MonoBehaviour
{
    private string accountId;

    public Text mintedToken;
    public Text currentBalance;
    public Text fuseResult;
    public Text listToken;
    public Text nftDetails;
    public PlayerConnector playerConnector;
    public ContractCaller contractCaller;

    public Button connectButton;
    public Button mintTokenAButton;
    public Button mintTokenBButton;
    public Button mintTokenAIfDoesNotExistButton;
    public Button randomLootButton;
    public Button mintFTTokenButton;
    public Button fuseButton;
    public Button getNFTBlockchainButton;
    public Button getNFTAPIButton;

    public InputField tokenFuse1;
    public InputField tokenFuse2;
    public InputField nftInputField;
    public InputField tokenFuseTarget;

    private string nftContractId = "dev-1643368771086-83655298164012";
    private string ftContractId = "dev-1642749960038-75584185908370";

    private string tokenSeriesIdA = "1";
    private string tokenSeriesIdB = "2";

    private string parasAPI = "https://api-v2-testnet.paras.id";

    private string fusingPrice = "100000000000000000000";

    void Awake()
    {
        // Finding Action Components if they are not drag'n'dropped on the script
        if (playerConnector == null) playerConnector = GameObject.Find("Actions/ConnectPlayer").GetComponent<PlayerConnector>();
        if (contractCaller == null) contractCaller = GameObject.Find("Actions/CallContract").GetComponent<ContractCaller>();

        if (playerConnector.IsPlayerConnected()) accountId = playerConnector.GetPlayerAccountId();
        ChangeUI(playerConnector.IsPlayerConnected());
    }
    void Start()
    {
        Debug.Log($@"ConnectPlayer is found ({playerConnector != null}) | CallContract is found ({contractCaller != null})");

        connectButton.onClick.AddListener(OnConnectPlayer);
        mintTokenAButton.onClick.AddListener(OnMintTokenA);
        mintTokenBButton.onClick.AddListener(OnMintTokenB);
        mintTokenAIfDoesNotExistButton.onClick.AddListener(OnMintTokenAIfDoesNotExist);
        randomLootButton.onClick.AddListener(OnRandomLoot);
        mintFTTokenButton.onClick.AddListener(OnMintFTToken);
        fuseButton.onClick.AddListener(OnFuse);
        getNFTBlockchainButton.onClick.AddListener(OnGetNFTBlockchain);
        getNFTAPIButton.onClick.AddListener(OnGetNFTDetailAPI);
    }

    async void MintTokenNFT(string tokenSeriesId) {
        Debug.Log(accountId + " is minting an NFT");

        dynamic arguments = new ExpandoObject();
        arguments.token_series_id = tokenSeriesId;
        arguments.receiver_id = accountId;

        ulong nearGas = (ulong)UnitConverter.GetGasFormat(30);
        UInt128 yoctoNearDeposit = (UInt128)UnitConverter.GetYoctoNearFormat(0);

        Debug.Log("yoctoNearDeposit: " + yoctoNearDeposit);

        mintedToken.text = "Minted token :" + "Please wait ...";
        dynamic result = await contractCaller.CallContract(nftContractId, "nft_buy", arguments, EConnectionActor.Player, nearGas, yoctoNearDeposit);
        Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));
        mintedToken.text = "Minted token :" + JsonConvert.SerializeObject(result);
    }

    async void viewAndSetAccountBalance() {
        Debug.Log("Viewing FT balance");
        dynamic arguments = new ExpandoObject();
        arguments.account_id = accountId;
        dynamic result = await contractCaller.ViewContract(ftContractId, "ft_balance_of", arguments, EConnectionActor.Player);
        Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));

        BigInteger balance = BigInteger.Parse((string)result.result);
        BigInteger decimals = BigInteger.Parse("1000000000000000000"); // 10**18
        currentBalance.text = "Current balance: " + balance / decimals + " COIN";
    }

    async void OnConnectPlayer()
    {
        if (!playerConnector.IsPlayerConnected())
        {
            await playerConnector.ConnectWalletByBrowserAsync();
            accountId = playerConnector.GetPlayerAccountId();
        }
        else
        {
            playerConnector.DisconnectWallet();
            accountId = null;
        }
        ChangeUI(playerConnector.IsPlayerConnected());
    }

    async void OnMintTokenA() {
        MintTokenNFT(tokenSeriesIdA);
    }

    async void OnMintTokenB() {
        MintTokenNFT(tokenSeriesIdB);
    }

    async void OnGetNFTBlockchain() {
        dynamic arguments = new ExpandoObject();
        arguments.account_id = accountId;
        dynamic result = await contractCaller.ViewContract(nftContractId, "nft_supply_for_owner", arguments, EConnectionActor.Player);
        Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));

        int totalNftforOwner = int.Parse(result.result);
        string resultText = "";
        Debug.Log(totalNftforOwner);
        if (totalNftforOwner > 0) {
            Debug.Log("Checking if user has token A");
            for (int i = 0; i < totalNftforOwner/10 + 1; i++) {
                dynamic argumentsI = new ExpandoObject();
                argumentsI.account_id = accountId;
                argumentsI.start_index = i * 10;
                argumentsI.limit = (i+1) * 10;
                dynamic resultI = await contractCaller.ViewContract(nftContractId, "nft_tokens_for_owner", arguments, EConnectionActor.Player);
                Debug.Log(JsonConvert.SerializeObject(resultI));
                dynamic resultJson = JsonConvert.DeserializeObject(resultI.result);
                for (int j = 0; j < resultJson.Count; j++) {
                    resultText += resultJson[j].token_id + ",";
                }
            }
        }
        listToken.text = "List NFT: " + resultText;
    }

    async void OnGetNFTDetailAPI() {
        string tokenId = nftInputField.text;
        string path = "/token?";
        path += "contract_id=" + nftContractId; 
        path += "&";
        path += "token_id" + tokenId;
        StartCoroutine(GetRequest(parasAPI + path, (UnityWebRequest req) => {
            if (req.isNetworkError || req.isHttpError) {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
            } else {
                Debug.Log(req.downloadHandler.text);
                nftDetails.text = req.downloadHandler.text;
            }
        }));
    }

    async void OnMintTokenAIfDoesNotExist() {
        dynamic arguments = new ExpandoObject();
        arguments.account_id = accountId;
        dynamic result = await contractCaller.ViewContract(nftContractId, "nft_supply_for_owner", arguments, EConnectionActor.Player);
        Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));

        int totalNftforOwner = int.Parse(result.result);
        Debug.Log(totalNftforOwner);
        if (totalNftforOwner > 0) {
            // Mint token A
            Debug.Log("Checking if user has token A");
            for (int i = 0; i < totalNftforOwner/10 + 1; i++) {
                dynamic argumentsI = new ExpandoObject();
                argumentsI.account_id = accountId;
                argumentsI.start_index = i * 10;
                argumentsI.limit = (i+1) * 10;
                dynamic resultI = await contractCaller.ViewContract(nftContractId, "nft_tokens_for_owner", arguments, EConnectionActor.Player);
                Debug.Log(JsonConvert.SerializeObject(resultI));
                dynamic resultJson = JsonConvert.DeserializeObject(resultI.result);
                Debug.Log(resultJson[0].token_id);
                for (int j = 0; j < resultJson.Count; j++) {
                    string tokenSeriesId = ((string)resultJson[j].token_id).Split(':')[0];
                    if (tokenSeriesId == tokenSeriesIdA) {
                        mintedToken.text = "Minted token:" + " You already have token A";
                        return;
                    }
                }
            }
        } 
        MintTokenNFT(tokenSeriesIdA);
    }

    async void OnRandomLoot() {
        Debug.Log(accountId + " is minting an NFT");

        dynamic arguments = new ExpandoObject();
        ulong nearGas = (ulong)UnitConverter.GetGasFormat(30);
        UInt128 yoctoNearDeposit = (UInt128)UnitConverter.GetYoctoNearFormat(0);

        Debug.Log("yoctoNearDeposit: " + yoctoNearDeposit);

        mintedToken.text = "Minted token :" + "Please wait ...";
        dynamic result = await contractCaller.CallContract(nftContractId, "nft_random_loot", arguments, EConnectionActor.Player, nearGas, yoctoNearDeposit);
        Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));
        mintedToken.text = "Minted token :" + JsonConvert.SerializeObject(result);
    }

    async void OnMintFTToken() {
        Debug.Log(accountId + " is minting an FT");

        dynamic arguments = new ExpandoObject();
        arguments.receiver_id = accountId;

        ulong nearGas = (ulong)UnitConverter.GetGasFormat(30);
        UInt128 yoctoNearDeposit = (UInt128)UnitConverter.GetYoctoNearFormat(0);

        Debug.Log("yoctoNearDeposit: " + yoctoNearDeposit);

        dynamic result = await contractCaller.CallContract(ftContractId, "mint_tokens", arguments, EConnectionActor.Player, nearGas, yoctoNearDeposit);
        Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));
        viewAndSetAccountBalance();
    }

    async void OnFuse() {
        string tokenA = tokenFuse1.text;
        string tokenB = tokenFuse2.text;
        string targetTokenSeriesId = tokenFuseTarget.text;

        Debug.Log(accountId + " is fusing an NFT");

        dynamic arguments = new ExpandoObject();
        arguments.receiver_id = nftContractId;
        arguments.amount = fusingPrice;
        arguments.msg =  // recode this using Json
            "{\"token_ids\":[\"" + 
            tokenA +
            "\",\"" +
            tokenB +
            "\"]," +
            "\"target_token_series_id\":" +
            "\"" +
            targetTokenSeriesId + 
            "\"}";
        ulong nearGas = (ulong)UnitConverter.GetGasFormat(300);
        UInt128 yoctoNearDeposit = (UInt128)UnitConverter.GetYoctoNearFormat(0);

        Debug.Log("yoctoNearDeposit: " + yoctoNearDeposit);

        mintedToken.text = "Fusing NFT:" + "Please wait ...";
        dynamic result = await contractCaller.CallContract(ftContractId, "ft_transfer_call_unsafe", arguments, EConnectionActor.Player, nearGas, yoctoNearDeposit);
        Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result));

        dynamic arguments2 = new ExpandoObject();
        arguments2.account_id = accountId;
        dynamic result2 = await contractCaller.ViewContract(nftContractId, "get_last_token_by_owner", arguments2, EConnectionActor.Player);
        Debug.Log("Blockchain has returned the result of contract calling: " + JsonConvert.SerializeObject(result2));

        fuseResult.text = "Minted: " + JsonConvert.SerializeObject(result2.result);
    }

    private void ChangeUI(bool connected)
    {
        if (connected)
        {
            connectButton.GetComponentInChildren<Text>().text = "Disconnect: " + accountId;
            mintTokenAButton.gameObject.SetActive(true);
            mintTokenBButton.gameObject.SetActive(true);
            mintTokenAIfDoesNotExistButton.gameObject.SetActive(true);
            randomLootButton.gameObject.SetActive(true);
            mintFTTokenButton.gameObject.SetActive(true);
            fuseButton.gameObject.SetActive(true);
            viewAndSetAccountBalance();
        }
        else
        {
            connectButton.GetComponentInChildren<Text>().text = "Connect Player";
            mintTokenAButton.gameObject.SetActive(false);
            mintTokenBButton.gameObject.SetActive(false);
            mintTokenAIfDoesNotExistButton.gameObject.SetActive(false);
            randomLootButton.gameObject.SetActive(false);
            mintFTTokenButton.gameObject.SetActive(false);
            fuseButton.gameObject.SetActive(false);
        }
    }

    IEnumerator GetRequest(string url, Action<UnityWebRequest> callback)
    {
        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            // Send the request and wait for a response
            yield return request.SendWebRequest();
            callback(request);
        }
    }

}