using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class GameEvents : MonoBehaviour
	{
		public static event Action onPlayerDead;
		public static event Action onReachFinish;
		public static event Action onGameStart;
		public static event Action onGamePause;
		public static event Action onGameOver;

		public static void OnPlayerDead() => onPlayerDead?.Invoke();
		public static void OnReachFinish() => onReachFinish?.Invoke();
		public static void OnGameStart() => onGameStart?.Invoke();
		public static void OnGamePause() => onGamePause?.Invoke();
		public static void OnGameOver() => onGameOver?.Invoke();
	}
}
