namespace QuantumArk
{
    public enum Allegiance
    {
        None,
        Player,
        Enemy,
        Ground,
        Obstacle,
		PlayerDamager,
		EnemyDamager,
	}
}