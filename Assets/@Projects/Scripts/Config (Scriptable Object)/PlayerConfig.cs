﻿
using UnityEngine;

namespace QuantumArk
{
	[CreateAssetMenu(fileName = "PlayerConfig", menuName = "Config/Player Config")]
	public class PlayerConfig : ScriptableObject
	{
		public float DashCooldown;
		public float DashDuration;
		public float SwitchWeaponCooldown;
		public float invulnerableOnDamagedDuration;
	}
}