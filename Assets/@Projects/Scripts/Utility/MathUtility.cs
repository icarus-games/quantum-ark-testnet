using System;
using UnityEngine;

namespace QuantumArk
{
    public class MathUtility
    {
        public static Vector2 RadianToVector2(float radian)
        {
            return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
        }

        public static Vector2 DegreeToVector2(float degree)
        {
            return RadianToVector2(degree * Mathf.Deg2Rad);
        }

        public static float FindAngleFromPoint(float pointY, float pointX)
        {
            var angle = Mathf.Atan2(pointY, pointX) * Mathf.Rad2Deg;
            if (angle < 0) angle += 360;
            return angle;
        }

        public static float FindAngleBetweenTwoPoints(Vector2 pointFrom, Vector3 pointTo)
        {
            var angle = Mathf.Atan2(pointFrom.y - pointTo.y, pointFrom.x - pointTo.x) * Mathf.Rad2Deg;
            // if (angle < 0) angle += 360f;

            return angle;
        }
    }
}