using Lean.Pool;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class EnemyAI : MonoBehaviour
	{
		[Header("Dependencies")]
		public EnemyControl control;
		public Rigidbody2D rb2d;
		public float speed = 2f;

		[Header("Patrol Config")]
		public Transform[] patrolPoints;
		private int currentPatrolIdx;
		private Vector3[] cachePatrolTransform;
		public float[] patrolWaitDelay = new float[] { 0f, 0f, 1f, 2f };

		[Header("Chase Config")]
		private GameObject playerObject;

		[Header("Auto-Shoot Config")]
		public float shootAreaRadius = 5f;
		public Weapon enemyWeapon;
		public Transform weaponParent;
		private bool isWeaponSpawned = false;
		public bool aimPlayer = false;

		[Header("Burst Shoot Config")]
		public bool isBurstShooting = false;
		public int shootAmountBeforeDelay = 3;
		public float burstDelay = 2f;
		private int shootAmount = 0;
		public bool canShoot = true;

		private IEnumerator Start()
		{
			while(playerObject == null)
			{
				yield return null;
				playerObject = GameObject.FindGameObjectWithTag("Player");
			}
		}

		#region PATROL
		public Action onReachPatrolPoint;

		public void InitPatrolPoint()
		{
			currentPatrolIdx = 0;
			cachePatrolTransform = new Vector3[patrolPoints.Length];
			for (int i = 0; i < patrolPoints.Length; i++)
			{
				var p = patrolPoints[i].position;
				cachePatrolTransform[i] = new Vector3(p.x, p.y, p.z);
			}
		}

		public Vector3 GetPatrolPoint() => cachePatrolTransform[currentPatrolIdx];

		public void Patrol()
		{
			if (Vector2.Distance(transform.position, GetPatrolPoint()) < 0.2f)
			{
				if (currentPatrolIdx < patrolPoints.Length - 1) currentPatrolIdx++;
				else currentPatrolIdx = 0;
				onReachPatrolPoint?.Invoke();
			}
			control.MoveTowards(GetPatrolPoint());
		}

		public void UpdateFacePatrol()
		{
			UpdateFace(GetPatrolPoint());
		}

		public float WaitTimePatrol()
		{
			return patrolWaitDelay[UnityEngine.Random.Range(0, patrolWaitDelay.Length)];
		}
		#endregion

		#region CHASE
		public void ChasePlayer()
		{
			if (playerObject == null) return;
			if (Vector2.Distance(transform.position, playerObject.transform.position) > 0.01f)
			{
				control.MoveTowardsOnAir(PlayerHeadPos());
			}
		}
		#endregion

		#region AUTO-SHOOT

		public bool IsOnShootArea() => Vector2.Distance(transform.position, playerObject.transform.position) < shootAreaRadius;

		public void TrySingleShoot() => enemyWeapon.TryShoot();

		public void SetOnShoot(Action act) => enemyWeapon.OnShoot = act;

		public void SpawnSingleWeapon() => CreateWeapon(enemyWeapon);

		private void CreateWeapon(Weapon prefab)
		{
			if (isWeaponSpawned) return;
			isWeaponSpawned = true;

			enemyWeapon = LeanPool.Spawn(prefab, weaponParent);
			var wpnTrans = enemyWeapon.transform;
			wpnTrans.localPosition = Vector3.zero;
			wpnTrans.localRotation = Quaternion.identity;
		}

		public void UpdateAngle(float angleParam = -1f)
		{
			var angle = MathUtility.FindAngleBetweenTwoPoints(PlayerHeadPos(), weaponParent.position);
			var rotation = weaponParent.eulerAngles;
			rotation.z = angleParam == -1 ? angle : angleParam;
			weaponParent.eulerAngles = rotation;
		}
		
		public float UpdateFace(Vector3 pos)
		{
			var diff = transform.position - pos;
			var scale = transform.localScale;
			scale.x = diff.x >= 0f ? 1 : -1;
			transform.localScale = scale;
			return scale.x;
		}

		public void UpdateFaceTowardsPlayer()
		{
			UpdateFace(playerObject.transform.position);
		}

		public void UpdateFaceShoot()
		{
			var x = UpdateFace(playerObject.transform.position);
			UpdateAngle(x >= 0f ? 180 : 0);
		}

		public Vector3 PlayerHeadPos()
		{
			Vector3 playerPos = playerObject.transform.position;
			Vector3 adjustedPlayerPos = new Vector3(playerPos.x, playerPos.y + 2f, playerPos.z);
			return adjustedPlayerPos;
		}

		public bool HasFoundPlayer() => playerObject != null;

		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(transform.position, shootAreaRadius);
		}
		#endregion

		public void HandleBurstShoot()
		{
			if (shootAmount >= shootAmountBeforeDelay)
			{
				StartCoroutine(CooldownBurst());
			}
		}

		public void OnShoot()
		{
			shootAmount++;
			if (isBurstShooting) HandleBurstShoot();
		}

		private IEnumerator CooldownBurst()
		{
			canShoot = false;
			yield return new WaitForSeconds(burstDelay);
			canShoot = true;
			shootAmount = 0;
		}
	}
}
