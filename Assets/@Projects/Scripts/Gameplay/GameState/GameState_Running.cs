using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace QuantumArk
{
	public class GameState_Running : BaseGameState
	{
		public GameState_Running(GameStateFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			GameEvents.onPlayerDead += OnPlayerDead;
			GameEvents.onReachFinish += OnReachFinish;
		}

		public override void OnExit()
		{
			base.OnExit();
			Cursor.visible = true;
			GameEvents.onPlayerDead -= OnPlayerDead;
			GameEvents.onReachFinish -= OnReachFinish;
		}

		private void OnPlayerDead()
		{
			fsm.ChangeState(new GameState_GameOver(fsm));
		}

		private void OnReachFinish()
		{
			fsm.ChangeState(new GameState_Complete(fsm));
		}
	}
}
