using QuantumArk.Blockchain;
using UnityEngine;

namespace QuantumArk.UI
{
    public class ErrorToastListener : MonoBehaviour
    {
        private void OnEnable()
        {
            BlockchainManager.OnCallException += OnExceptionCalled;
        }

        private void OnDisable()
        {
            BlockchainManager.OnCallException -= OnExceptionCalled;
        }

        private void OnExceptionCalled(string message)
        {
            ErrorToastMenu.Open(message);
        }
    }
}