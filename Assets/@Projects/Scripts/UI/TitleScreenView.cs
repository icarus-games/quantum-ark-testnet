using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace QuantumArk.UI
{
    public class TitleScreenView : MonoBehaviour
    {
        public Button connectWalletButton;
        public Button playButton;
        public Button inventoryButton;
        public Button logoutButton;
        public Button quitGameButton;

        [SerializeField] private Transform connectedPanel = null;
        [SerializeField] private TextMeshProUGUI playerAddressText = null;

        public void ChangeState(bool isConnected)
        {
            connectWalletButton.gameObject.SetActive(!isConnected);
            connectedPanel.gameObject.SetActive(isConnected);
            logoutButton.gameObject.SetActive(isConnected);
        }

        public void SetWalletAddress(string address)
        {
            playerAddressText.text = $"WELCOME, {address}!";
        }
    }
}