using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace QuantumArk.UI
{
    public class ResultItemView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI itemName = null;
        [SerializeField] private Image image = null;

        public void SetText(string text) => itemName.text = text;
        public void SetSprite(Sprite sprite) => image.sprite = sprite;

    }
}