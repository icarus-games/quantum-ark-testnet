using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public class PatrolState_Patrol : EnemyBaseState
	{
		public PatrolState_Patrol(PatrolFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			view.PlayRun();
			control.OnEnemyDeath(delegate { fsm.ChangeState(new PatrolState_Die((PatrolFSM)fsm)); });
			ai.onReachPatrolPoint = OnReachPatrol;
			//Observable.Timer(TimeSpan.FromSeconds(1f)).Subscribe(_ => ai.onReachPatrolPoint = OnReachPatrol).AddTo(disposable);
			Observable.EveryFixedUpdate().Subscribe(_ => ObserveUpdate()).AddTo(disposable);
		}

		private void ObserveUpdate()
		{
			ai.Patrol();
			ai.UpdateFacePatrol();
		}

		private void OnReachPatrol()
		{
			fsm.ChangeState(new PatrolState_Idle((PatrolFSM)fsm));
		}
	}
}
