using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk.Sandbox
{
    public class WeaponTestController : MonoBehaviour
    {
        [SerializeField] private Weapon weapon = null;
        [SerializeField] private RainOfBulletAbility rainOfBulletAbility = null;
        
        private void Update()
        {
            if (Input.GetMouseButton(0))
            {
                if (weapon == null) weapon = FindObjectOfType<Weapon>();
                weapon?.TryShoot();   
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                rainOfBulletAbility.ActivateAbility(transform);
            }
        }
    }

}