using System;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
    public class PlayerSpecialAttack : MonoBehaviour
    {
        [SerializeField] private RainOfBulletAbility ability = null;
        [SerializeField] private float cooldown = 0f;

        public FloatReactiveProperty time = new FloatReactiveProperty(0f);
        public bool IsReady => time.Value <= 0;

        public float Progress => (cooldown - time.Value) / cooldown;
		public Action OnAbilityActivated;

        private void Start()
        {
            ResetTimer();
        }

        public void ActivateSkill()
        {
            if (!IsReady) return;
            
            ability.ActivateAbility(transform);
			OnAbilityActivated?.Invoke();
            ResetTimer();
        }

        private void ResetTimer()
        {
            time.Value = cooldown;
        }

        private void Update()
        {
            if (IsReady) return;
            
            time.Value = Mathf.Clamp(time.Value - Time.deltaTime, 0f, cooldown);
        }

		public BulletSpawner BulletSpawner => ability.BulletSpawner;
    }
}