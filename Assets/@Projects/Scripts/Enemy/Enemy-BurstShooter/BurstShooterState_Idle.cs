using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public class BurstShooterState_Idle : EnemyBaseState
	{
		public BurstShooterState_Idle(BurstShooterFSM fsm) : base(fsm)
		{
		}


		public override void OnEnter()
		{
			view.PlayIdle();
			ai.SpawnSingleWeapon();
			control.OnEnemyDeath(delegate { fsm.ChangeState(new BurstShooterState_Die((BurstShooterFSM)fsm)); });
			Observable.EveryUpdate().Subscribe(_ => ObserveUpdate()).AddTo(disposable);
		}

		private void ObserveUpdate()
		{
			if (ai.IsOnShootArea())
			{
				fsm.ChangeState(new BurstShooterState_OnArea((BurstShooterFSM)fsm));
			}
		}
	}
}