using UnityEngine;

namespace QuantumArk
{
    [CreateAssetMenu(fileName = "CosmeticItemConfig", menuName = "QuantumArk/CosmeticItemConfig")]
    public class CosmeticItemConfig : ScriptableObject
    {
        public string assetId;
        public string itemName;
        public string description;
    }
}