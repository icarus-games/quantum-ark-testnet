using UnityEngine;

namespace QuantumArk
{
    public class CollisionResolver
    {
        public static void Resolve(GameObject source, GameObject target)
        {
            if (source.CompareTag("Damageable") && target.CompareTag("Damageable"))
            {
                var dmgSource = source.GetComponent<DamageSource>();
                var dmgTarget = target.GetComponent<DamageTarget>();

                if (dmgSource != null && dmgTarget != null)
                {
                    if (dmgTarget.CanByDamagedBy(dmgSource))
                    {
                        dmgTarget.Damage(dmgSource.damagePts);
                    }
                }
            }
        }
    }
}