using System;

namespace QuantumArk.Blockchain
{
    public static class BlockchainUtility
    {
        private static readonly double BaseNear = Math.Pow(10, 18);
        public static double GetNumberFromBaseNear(string amountString)
        {
            amountString = amountString.Replace("\"", "");
            return double.Parse(amountString) / BaseNear;
        }
    }
}