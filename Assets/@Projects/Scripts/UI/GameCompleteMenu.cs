using QuantumArk;
using UniRx;
using UnityEngine;

namespace QuantumArk.UI
{
	public class GameCompleteMenu : MonoBehaviour
	{
		[SerializeField] private GameCompleteView view = null;

		public void Show() => view.Show();

		public void Hide() => view.Hide();

		private void Start()
		{
			GameEvents.onGameOver += Show;
			view.restartButton.OnClickAsObservable().TakeUntilDisable(this).Subscribe(_ => RestartScene());
		}

		private void OnDestroy()
		{
			GameEvents.onGameOver -= Show;
		}

		private void RestartScene()
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene(1);
		}
	}
}
