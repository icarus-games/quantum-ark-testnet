using System;
using Lean.Pool;
using UnityEngine;

namespace QuantumArk
{
    public class BulletSpawner : MonoBehaviour
    {
        [SerializeField] private Bullet bulletPrefab = null;

		private Transform myTransform => this.transform;

		public Action OnBulletSpawned;
        //private void Awake()
        //{
        //    myTransform = transform;
        //}

        public void Spawn(float damage, float extraAngle)
        {
            var bullet = LeanPool.Spawn(bulletPrefab, myTransform.position, myTransform.rotation);

            var eulerAngles = bullet.transform.eulerAngles;
            if (extraAngle != 0)
            {
                eulerAngles.z += extraAngle;
                bullet.transform.eulerAngles = eulerAngles;
            }
			OnBulletSpawned?.Invoke();
            bullet.Launch(eulerAngles.z);
            bullet.SetDamage(damage);
        }
    }
}