using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public class PatrolState_Die : EnemyBaseState
	{
		public PatrolState_Die(PatrolFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			view.PlayDeath();
		}
	}
}
