﻿using UniRx;

namespace QuantumArk
{
	public abstract class BasePlayerState : IState
	{
		protected PlayerFSM fsm;

		protected PlayerControl control;
		protected PlayerView view;
		protected PlayerInput input;
		protected CompositeDisposable disposable;

		public BasePlayerState(PlayerFSM fsm)
		{
			this.fsm = fsm;
			this.control = fsm.control;
			this.view = fsm.view;
			this.input = fsm.input;
			disposable = new CompositeDisposable();
		}

		~BasePlayerState()
		{
		}

		public abstract void OnEnter();

		public virtual void OnExit()
		{
			if (!disposable.IsDisposed) disposable.Dispose();
		}

		public virtual void OnUpdate()
		{
		}

		protected void ChangeStateToIdle()
		{
			fsm.ChangeState(new PlayerState_Idle(fsm));
		}

		protected void ChangeStateToDie()
		{
			fsm.ChangeState(new PlayerState_Die(fsm));
		}

		protected void Jump()
		{
			control.Jump();
			view.PlayJump();
			fsm.ChangeState(new PlayerState_Jump(fsm));
		}

		protected void Dash()
		{
			if (control.IsDashCooldown) return;
			view.PlayDash();
			control.DisableBodyTrigger(fsm.config.DashDuration);
			fsm.ChangeState(new PlayerState_Dash(fsm));
		}

		protected void Shoot(bool isShooting)
		{
			control.IsShooting.Value = isShooting;
			if (!isShooting) return;
			control.TryShoot();
		}

		protected void SpecialAttack()
		{
			if (!control.IsUltimateReady()) return;
			fsm.ChangeState(new PlayerState_Ultimate(fsm));
		}
	}
}
