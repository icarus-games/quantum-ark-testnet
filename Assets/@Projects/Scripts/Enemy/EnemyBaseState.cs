using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public abstract class EnemyBaseState : IState
	{
		protected EnemyFSM fsm;

		protected EnemyControl control;
		protected EnemyView view;
		protected EnemyAI ai;

		protected CompositeDisposable disposable = new CompositeDisposable();

		public EnemyBaseState(EnemyFSM fsm)
		{
			this.fsm = fsm;
			this.control = fsm.control;
			this.view = fsm.view;
			this.ai = fsm.ai;
		}

		~EnemyBaseState()
		{
		}

		public abstract void OnEnter();

		public virtual void OnExit()
		{
			if (!disposable.IsDisposed) disposable.Dispose();
		}

		public virtual void OnUpdate()
		{
		}
	}
}
