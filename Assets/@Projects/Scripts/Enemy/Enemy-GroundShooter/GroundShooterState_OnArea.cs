using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public class GroundShooterState_OnArea : EnemyBaseState
	{
		public GroundShooterState_OnArea(GroundShooterFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			ai.SetOnShoot(view.PlayShoot);
			control.OnEnemyDeath(delegate { fsm.ChangeState(new GroundShooterState_Die((GroundShooterFSM)fsm)); });
			Observable.EveryFixedUpdate().Subscribe(_ => ObserveUpdate()).AddTo(disposable);
		}

		private void ObserveUpdate()
		{
			if (!ai.IsOnShootArea())
			{
				fsm.ChangeState(new GroundShooterState_Idle((GroundShooterFSM)fsm));
			}
			ai.UpdateFaceShoot();
			if (ai.aimPlayer) ai.UpdateAngle();
			ai.TrySingleShoot();
		}
	}
}
