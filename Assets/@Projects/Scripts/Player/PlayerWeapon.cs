using System;
using Lean.Pool;
using Spine.Unity;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
    public class PlayerWeapon : MonoBehaviour
    {
        [SerializeField] private Weapon[] weaponPrefabs = null;
        [SerializeField] private SkeletonAnimation spine = null;
        [SerializeField] private Transform weaponParent = null;
        [SerializeField] private MuzzleAimCrosshair muzzleAimCrosshair = null;

        private Weapon activeWeapon = null;
        public Weapon ActiveWeapon => activeWeapon;

        public IntReactiveProperty activeWeaponIndex = new IntReactiveProperty(0);

        private void Start()
        {
            CreateWeapon(weaponPrefabs[activeWeaponIndex.Value]);
            muzzleAimCrosshair.SetSkeleton(spine);
        }

        public void CreateWeapon(Weapon prefab)
        {
            if (activeWeapon) LeanPool.Despawn(activeWeapon);

            activeWeapon = LeanPool.Spawn(prefab, weaponParent);
            var wpnTrans = activeWeapon.transform;
            wpnTrans.localPosition = Vector3.zero;
            wpnTrans.localRotation = Quaternion.identity;
			activeWeapon.OnShoot = delegate {
				if (string.IsNullOrEmpty(activeWeapon.shotSfx)) return;
				AudioPlayer.Instance.PlaySfx(activeWeapon.shotSfx);
				CameraFx.Instance.Shake(CameraShakePreset.Mild);
			};
        }

        public void SwitchWeapon()
        {
            activeWeaponIndex.Value = activeWeaponIndex.Value == 0 ? 1 : 0;
            CreateWeapon(weaponPrefabs[activeWeaponIndex.Value]);
        }
    }
}