using System;
using UnityEngine;

namespace QuantumArk
{
    public class FollowMouseCrosshair : MonoBehaviour
    {
        private MouseCrosshair mouseCrosshair = null;
        private Transform myTransform;
        
        private void Awake()
        {
            myTransform = transform;
        }

        private void TryGetMouseCrosshair()
        {
            if (mouseCrosshair == null) mouseCrosshair = FindObjectOfType<MouseCrosshair>();
        }
        
        private void Update()
        {
            TryGetMouseCrosshair();
            if (mouseCrosshair == null) return;

            var thisPos = myTransform.position;
            var crosshairPos = mouseCrosshair.transform.position;

            var angle = MathUtility.FindAngleBetweenTwoPoints(crosshairPos, thisPos);

            var rotation = myTransform.eulerAngles;
            rotation.z = angle;
            myTransform.eulerAngles = rotation;
        }
    }
}