using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class GameState_Pause : BaseGameState
	{
		public GameState_Pause(GameStateFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			GameEvents.OnGamePause();
		}
	}
}

