using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace QuantumArk.UI
{
    public class ResultView : MonoBehaviour
    {
        public Button nextButton = null;
        public TextMeshProUGUI resultText = null;

        [SerializeField] private ResultItemView nftItem = null;
        [SerializeField] private ResultItemView coinItem = null;

        public void Initialize(string title, CosmeticItem cosmeticItem, string coinAmount)
        {
            resultText.text = title;
            coinItem.gameObject.SetActive(!string.IsNullOrEmpty(coinAmount));
            coinItem.SetText($"{coinAmount} GEN");
            
            nftItem.SetText(cosmeticItem.FullItemName);
            nftItem.SetSprite(Resources.Load<Sprite>( $"CosmeticItem/{cosmeticItem.AssetId}"));
        }
    }
}