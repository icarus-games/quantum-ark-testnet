using System;
using Spine;
using Spine.Unity;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
    public class MuzzleAimCrosshair : MonoBehaviour
    {
        [SerializeField] private SkeletonAnimation skeletonAnimation;

        [SerializeField] private Transform facingAngleRoot = null;
        
        [SpineBone(dataField: "skeletonAnimation")]
        [SerializeField] private string muzzleBoneName;
        
        [SpineBone(dataField: "skeletonAnimation")]
        [SerializeField] private string crosshairBoneName;

        private Bone muzzleBone;
        private Bone crosshairBone;
        private Transform myTransform;
        private Vector3 muzzlePos, crosshairPos;

        public IntReactiveProperty facingSign = new IntReactiveProperty(0);
        public BoolReactiveProperty isCursorVisible = new BoolReactiveProperty(false);
        
        private void OnValidate()
        {
            if (skeletonAnimation == null) skeletonAnimation = GetComponent<SkeletonAnimation>();
        }

        private void Awake()
        {
            myTransform = transform;
        }

        private void Start()
        {
            if (skeletonAnimation != null)
            {
                SetSkeleton(skeletonAnimation);
            }
            isCursorVisible.TakeUntilDisable(this).Subscribe(SetCursorVisibility);
        }

        private void SetCursorVisibility(bool val)
        {
            Cursor.visible = val;
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            isCursorVisible.Value = !hasFocus;
        }

        public void SetSkeleton(SkeletonAnimation skeletonAnimation)
        {
            this.skeletonAnimation = skeletonAnimation;
            muzzleBone = skeletonAnimation.Skeleton.FindBone(muzzleBoneName);
            crosshairBone = skeletonAnimation.skeleton.FindBone(crosshairBoneName);
            
            muzzleBone.UpdateWorldTransform();
            crosshairBone.UpdateWorldTransform();
        }

        private void Update()
        {
            if (skeletonAnimation == null) return;

            if (muzzleBone == null || crosshairBone == null)
            {
                SetSkeleton(skeletonAnimation);
            }
            
            UpdateBone();
            UpdateTransform();
            UpdateFacing();
        }

        private void UpdateBone()
        {
            muzzleBone.UpdateWorldTransform();
            muzzlePos = muzzleBone.GetWorldPosition(skeletonAnimation.transform);

            crosshairBone.UpdateWorldTransform();
            crosshairPos = crosshairBone.GetWorldPosition(skeletonAnimation.transform);
        }

        private void UpdateTransform()
        {
            // the updated position from spine returns NaN in the first few frames, so skip if it appears
            if (float.IsNaN(muzzlePos.x)) return;
            
            var muzzleAngle = MathUtility.FindAngleBetweenTwoPoints(crosshairPos, muzzlePos);
            myTransform.position = muzzlePos;

            var rotation = myTransform.eulerAngles;
            rotation.z = muzzleAngle;
            myTransform.eulerAngles = rotation;
        }

        private void UpdateFacing()
        {
            var facingAngle = MathUtility.FindAngleBetweenTwoPoints(crosshairPos, facingAngleRoot.position);
            if (facingAngle <= 0) facingAngle += 360f;

            var newFaceSign = 0;
            if (facingAngle > 90 && facingAngle <= 270)
            {
                newFaceSign = -1;
                facingAngle = Mathf.Clamp(facingAngle, 90f, 269f);
            }
            else
            {
                newFaceSign = 1;
            }

            if (newFaceSign != facingSign.Value)
            {
                facingSign.Value = newFaceSign;
            }
        }
    }
}