using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace QuantumArk.UI
{
    public class LoadoutMenu : Menu<LoadoutMenu>
    {
        [SerializeField] private LoadoutView view = null;
        [SerializeField] private string mainGameSceneName;

        public override async void Initialize()
        {
            LoadingMenu.Open();
            await Blackboard.Instance.FetchLatestData();
            LoadingMenu.Close();
            
            view.startButton.OnClickAsObservable().TakeUntilDisable(this).Subscribe(OnStartClicked);
            view.homeButton.OnClickAsObservable().TakeUntilDisable(this).Subscribe(_ => OnBackPressed());
            
            view.SetEquippedItem(Blackboard.Instance.EquippedItem);
        }

        private void OnStartClicked(Unit obj)
        {
            SceneManager.LoadScene(mainGameSceneName);
        }

        public override void OnBackPressed()
        {
            TitleScreenMenu.Open();
        }
    }
}