using System;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public class PatrolState_Idle : EnemyBaseState
	{
		public PatrolState_Idle(PatrolFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			view.PlayIdle();
			control.OnEnemyDeath(delegate { fsm.ChangeState(new PatrolState_Die((PatrolFSM)fsm)); });
			ChangeToPatrol();
		}

		private void ChangeToPatrol()
		{
			Observable.Timer(TimeSpan.FromSeconds(ai.WaitTimePatrol()))
				.Subscribe(_ => fsm.ChangeState(new PatrolState_Patrol((PatrolFSM)fsm)))
				.AddTo(disposable);
		}
	}
}
