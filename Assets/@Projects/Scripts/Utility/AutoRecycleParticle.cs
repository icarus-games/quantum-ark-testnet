using System;
using Lean.Pool;
using UnityEngine;

namespace QuantumArk
{
    public class AutoRecycleParticle : MonoBehaviour
    {
        [SerializeField] private new ParticleSystem particleSystem = null;

        private void OnValidate()
        {
            if (particleSystem == null) particleSystem = GetComponent<ParticleSystem>();
            
            if (particleSystem) CheckStopActionType();
        }

        private void Awake()
        {
            CheckStopActionType();
        }

        private void CheckStopActionType()
        {
            if (particleSystem.main.stopAction != ParticleSystemStopAction.Callback)
            {
                Debug.LogWarning( $"ParticleSystem {this}'s stopAction is not Callback. Particle won't be auto-recycled.");
            }
        }

        private void OnParticleSystemStopped()
        {
            LeanPool.Despawn(gameObject);
        }
    }
}