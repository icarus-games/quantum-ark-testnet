using System;
using System.Collections;
using UnityEngine;

namespace QuantumArk.Blockchain
{
    public interface IBlockchainApi
    {
        void ConnectWallet(Action callback);
        bool IsWalletConnected { get; }
        string WalletAddress { get; }
    }

    public class EditorBlockchainApi : MonoBehaviour, IBlockchainApi
    {
        public void ConnectWallet(Action callback)
        {
            StartCoroutine(_ConnectWallet());
            IEnumerator _ConnectWallet()
            {
                yield return new WaitForSeconds(1f);
                callback?.Invoke();
            }
        }

        public bool IsWalletConnected => true;
        public string WalletAddress => "testwallet.unityeditor";
    }
}