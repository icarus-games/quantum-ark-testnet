using System;
using Lean.Pool;
using UnityEngine;

namespace QuantumArk
{
    public class TimedRecycle : MonoBehaviour
    {
        [SerializeField] private float recycleTime = 0f;

        private float spawnTime = 0f;

        private void OnEnable()
        {
            spawnTime = Time.time;
        }

        private void Update()
        {
            if (recycleTime <= 0f) return;
            
            if (Time.time - spawnTime >= recycleTime)
            {
                LeanPool.Despawn(gameObject);
            }
        }
    }
}