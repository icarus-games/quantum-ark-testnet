using System;
using Spine;
using Spine.Unity;
using UnityEngine;

namespace QuantumArk
{
    public class MouseCrosshair : MonoBehaviour
    {
        [SerializeField] private SkeletonAnimation skeletonAnimation;

        [SpineBone(dataField: "skeletonAnimation")]
        [SerializeField] private string boneName;
        [SerializeField] private Camera mainCamera;
        
        private Bone bone;
        private Bounds cameraBounds;

        private void OnValidate()
        {
            if (skeletonAnimation == null) skeletonAnimation = GetComponent<SkeletonAnimation>();
        }

        private void Start()
        {
            bone = skeletonAnimation.Skeleton.FindBone(boneName);
        }

        private void Update()
        {
            // get mouse position as world position
            var worldMousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            
            // clamp to camera bounds
            GetCameraWorldPositionBounds(mainCamera, ref cameraBounds);
            worldMousePosition.x = Mathf.Clamp(worldMousePosition.x, cameraBounds.min.x, cameraBounds.max.x);
            worldMousePosition.y = Mathf.Clamp(worldMousePosition.y, cameraBounds.min.y, cameraBounds.max.y);
            
            // get skeleton space point and set it to the bone
            var skeletonSpacePoint = skeletonAnimation.transform.InverseTransformPoint(worldMousePosition);
            skeletonSpacePoint.x *= skeletonAnimation.Skeleton.ScaleX;
            skeletonSpacePoint.y *= skeletonAnimation.Skeleton.ScaleY;

            bone.SetLocalPosition(skeletonSpacePoint);
        }

        private Vector3 topRight = new Vector3(1, 1);
        private Vector3 botLeft = new Vector3(0, 0);

        private void GetCameraWorldPositionBounds(Camera camera, ref Bounds bounds)
        {
            var _topRightWorld = camera.ViewportToWorldPoint(this.topRight);
            var _bottomLeftWorld = camera.ViewportToWorldPoint(this.botLeft);
            
            bounds.SetMinMax(_bottomLeftWorld, _topRightWorld);
        }
    }
}