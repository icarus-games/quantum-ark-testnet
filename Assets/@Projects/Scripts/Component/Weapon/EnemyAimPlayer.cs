using Spine;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class EnemyAimPlayer : MonoBehaviour
	{
		[SerializeField] private SkeletonAnimation skeletonAnimation;

		[SpineBone(dataField: "skeletonAnimation")]
		[SerializeField] private string boneName;
		[SerializeField] private Transform target;
		[SerializeField] private EnemyAI ai;
		private Bone bone;

		private void OnValidate()
		{
			if (skeletonAnimation == null) skeletonAnimation = GetComponent<SkeletonAnimation>();
		}

		private void Start()
		{
			bone = skeletonAnimation.Skeleton.FindBone(boneName);
		}

		private void Update()
		{
			if (!ai.HasFoundPlayer()) return;
			var skeletonSpacePoint = skeletonAnimation.transform.InverseTransformPoint(ai.PlayerHeadPos());
			skeletonSpacePoint.x *= skeletonAnimation.Skeleton.ScaleX;
			skeletonSpacePoint.y *= skeletonAnimation.Skeleton.ScaleY;

			bone.SetLocalPosition(skeletonSpacePoint);
		}
	}
}
