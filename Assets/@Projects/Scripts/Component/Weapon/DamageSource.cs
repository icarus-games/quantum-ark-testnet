using UnityEngine;

namespace QuantumArk
{
    public class DamageSource : MonoBehaviour
    {
        public Allegiance allegiance;
        public float damagePts;
    }
}