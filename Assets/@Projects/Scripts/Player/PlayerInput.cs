using UnityEngine;
using System.Collections;
using System;
using UniRx;

namespace QuantumArk
{
	public class PlayerInput : MonoBehaviour
	{
		private Subject<Unit> onJumpPressed;
		private Subject<float> onMovePressed;
		private Subject<Unit> onDashPressed;
		private Subject<Unit> onCrouchPressed;
		private Subject<bool> onShootPressed;
		private Subject<Unit> onChangeWeaponPressed;
		private Subject<Unit> onSpecialAttackPressed;

		public IObservable<Unit> OnJumpPressed() { return onJumpPressed ?? (onJumpPressed = new Subject<Unit>()); }
		public IObservable<float> OnMovePressed() => onMovePressed ?? (onMovePressed = new Subject<float>());
		public IObservable<Unit> OnDashPressed() { return onDashPressed ?? (onDashPressed = new Subject<Unit>()); }
		public IObservable<Unit> OnCrouchPressed() { return onCrouchPressed ?? (onCrouchPressed = new Subject<Unit>()); }
		public IObservable<bool> OnShootPressed() { return onShootPressed ?? (onShootPressed = new Subject<bool>()); }
		public IObservable<Unit> OnChangeWeaponPressed() { return onChangeWeaponPressed ?? (onChangeWeaponPressed = new Subject<Unit>()); }
		public IObservable<Unit> OnSpecialAttackPressed() { return onSpecialAttackPressed ?? (onSpecialAttackPressed = new Subject<Unit>()); }

		public void Init()
		{
			Observable.EveryUpdate().TakeUntilDisable(this).Subscribe(_ => SubscribeObservableOnUpdate());
		}

		private void Start()
		{
			Init();
		}

		private void SubscribeObservableOnUpdate()
		{
			KeymapTrigger(KeyCode.Space, onJumpPressed);
			KeymapTrigger(KeyCode.LeftShift, onDashPressed);
			KeymapTrigger(KeyCode.S, onCrouchPressed);
			KeymapTrigger(KeyCode.Q, onChangeWeaponPressed);
			KeymapTrigger(KeyCode.Mouse1, onSpecialAttackPressed);
			
			GetXMoveValue();
			GetShootValue();
		}

		private void KeymapTrigger(KeyCode keyCode, Subject<Unit> action)
		{
			if (Input.GetKeyDown(keyCode))
			{
				action?.OnNext(Unit.Default);
			}
		}
		private void GetXMoveValue()
		{
			float result = Input.GetAxisRaw("Horizontal");
			onMovePressed?.OnNext(result);
		}

		private void GetShootValue()
		{
			onShootPressed?.OnNext(Input.GetButton("Fire1"));
		}
	}
}
