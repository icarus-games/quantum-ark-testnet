using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class BurstShooterState_Die : EnemyBaseState
	{
		public BurstShooterState_Die(BurstShooterFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			view.PlayDeath();
		}
	}
}