using System;
using System.Collections;
using System.Collections.Generic;
using QuantumArk;
using QuantumArk.Blockchain;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace QuantumArk.UI
{
    public class FuseView : MonoBehaviour
    {
        [SerializeField] private Transform itemParent = null;
        [SerializeField] private CosmeticItemView itemPrefab = null;
        [SerializeField] private FuseMaterialItem[] fuseMaterialItems = null;
        [SerializeField] private TextMeshProUGUI coinBalance = null;
        [SerializeField] private TextMeshProUGUI fuseCost = null;
        
        public List<CosmeticItem> selectedMaterials = new List<CosmeticItem>();
        
        public Button fuseButton = null;

        private List<CosmeticItemView> items = new List<CosmeticItemView>();
        
        private bool isInitialized = false;
        private Blackboard blackboard;
        private const int FuseMaterialCount = 2;

        public void Initialize(Blackboard blackboard)
        {
            this.blackboard = blackboard;
            
            UpdateTexts();
            StartCoroutine(UpdateInventoryItems());
            UpdateFrameButtons();
            ReleaseAllSelection();            
            UpdateFuseInteractable();
        }

        private void UpdateTexts()
        {
            fuseButton.interactable = false;
            coinBalance.text = $"x{blackboard.coins.Value}";
            fuseCost.text = $"x{BlockchainManager.Instance.FusingPrice}";
        }

        private IEnumerator UpdateInventoryItems()
        {
            for (int i = items.Count - 1; i >= 0; i--)
            {
                Destroy(items[i].gameObject);
            }

            yield return new WaitForEndOfFrame();
            var itemData = Blackboard.Instance.ownedItems;
            items = new List<CosmeticItemView>();
            
            for (int i = 0; i < itemData.Count; i++)
            {
                var cosmeticItem = itemData[i];
                var item = Instantiate(itemPrefab, itemParent);
                item.Initialize(cosmeticItem);
                item.actionButton.OnClickAsObservable().TakeUntilDestroy(item).Subscribe(_ => OnCosmeticItemClicked(item));
                
                items.Add(item);
            }
        }

        private void UpdateFrameButtons()
        {
            for (int i = 0; i < fuseMaterialItems.Length; i++)
            {
                var fuseItem = fuseMaterialItems[i];
                fuseItem.button.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(_ => ReleaseSelection(fuseItem));
            }
        }

        private void ReleaseSelection(FuseMaterialItem fuseItem)
        {
            if (!fuseItem.HasCosmeticItem) return;
            selectedMaterials.Remove(fuseItem.CosmeticItem);
            
            fuseItem.ItemView.gameObject.SetActive(true);
            fuseItem.ReleaseSelection();
            
            UpdateFuseInteractable();
        }

        private void ReleaseAllSelection()
        {
            for (int i = 0; i < fuseMaterialItems.Length; i++)
            {
                var fuseItem = fuseMaterialItems[i];
                ReleaseSelection(fuseItem);
            }
        }

        private void OnCosmeticItemClicked(CosmeticItemView itemView)
        {
            if (selectedMaterials.Count >= FuseMaterialCount) return;
            
            itemView.gameObject.SetActive(false);
            fuseMaterialItems[selectedMaterials.Count].ItemView = itemView;

            selectedMaterials.Add(itemView.cosmeticItem);
            UpdateFuseInteractable();
        }

        private void UpdateFuseInteractable()
        {
            fuseButton.interactable = selectedMaterials.Count == FuseMaterialCount;
        }
    }
}