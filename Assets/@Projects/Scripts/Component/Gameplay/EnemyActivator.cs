using System.Collections;
using UnityEngine;

namespace QuantumArk
{
	public class EnemyActivator : MonoBehaviour
	{
		[SerializeField] private float activateRadius = 5f;
		[SerializeField] private bool showGizmos = false;

		private GameObject playerObject;
		private bool hasActivated = false;
		private bool hasFoundPlayer = false;

		public EnemyFSM fsm;

		private IEnumerator Start()
		{
			while (playerObject == null)
			{
				yield return null;
				playerObject = GameObject.FindGameObjectWithTag("Player");
				hasFoundPlayer = true;
			}
		}

		private void Update()
		{
			if (hasFoundPlayer && !hasActivated && IsOnRadius())
			{
				fsm.Init();
				hasActivated = true;
			}
		}

		public bool IsOnRadius() => Vector2.Distance(transform.position, playerObject.transform.position) < activateRadius;

		private void OnDrawGizmosSelected()
		{
			if (!showGizmos) return;
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(transform.position, activateRadius);
		}
	}
}
