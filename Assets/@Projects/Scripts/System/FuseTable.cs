using System.Collections.Generic;

namespace QuantumArk
{
    public class FuseTable
    {
        public static readonly Dictionary<string, string> FuseResult = new Dictionary<string, string>()
        {
            {"1_1", "2"},
            {"1_2", "3"},
            {"1_3", "2"},
            {"2_1", "3"},
            {"2_2", "3"},
            {"2_3", "1"},
            {"3_1", "2"},
            {"3_2", "1"},
            {"3_3", "1"},
        };

        public static string GetFuseResult(CosmeticItem itemA, CosmeticItem itemB)
        {
            return FuseResult[$"{itemA.tokenId}_{itemB.tokenId}"];
        }
    }
}