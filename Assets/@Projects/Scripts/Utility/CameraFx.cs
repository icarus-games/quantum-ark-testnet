using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

namespace QuantumArk
{
    public class CameraFx : MonoBehaviourSingleton<CameraFx>
    {
        private Transform camParent;
        private Vector3 camParentInitialPos;

        public List<CameraShakePropertiesElement> shakePresets;
        private CameraShakePreset lastPreset;
        private bool isPlaying = false;

        public void Shake(CameraShakePreset preset)
        {
            var propElement = shakePresets.SingleOrDefault(x => x.preset == preset);
            if (propElement != default(CameraShakePropertiesElement))
            {
                var prop = propElement.properties;
                if(lastPreset > propElement.preset && isPlaying) return;
                lastPreset = propElement.preset;
                Shake(prop.duration, prop.strength, prop.vibrato, prop.randomness);
            }
        }
        
        private void Shake(float duration, float strength, int vibrato, float randomness)
        {
            if (camParent == null)
            {
                camParent = Camera.main.transform.parent;
                camParentInitialPos = camParent.transform.position;
            }
			camParent.DOShakePosition(duration, strength, vibrato, randomness)
                .OnStart(delegate { isPlaying = true; })
                .OnComplete(delegate { isPlaying = false; });
        }

		// private void Update()
		// {
		// 	if(Input.GetKeyDown(KeyCode.T)) Shake(CameraShakePreset.Mild);
		// 	if (Input.GetKeyDown(KeyCode.Y)) Shake(CameraShakePreset.Medium);
		// 	if (Input.GetKeyDown(KeyCode.U)) Shake(CameraShakePreset.Strong);
		// }
	}

    [Serializable]
    public class CameraShakePropertiesElement
    {
        public CameraShakePreset preset;
        public CameraShakeProperties properties;
    }
    
    [Serializable]
    public class CameraShakeProperties
    {
        public float duration;
        [Range(0, 1f)]
		public float strength;
        [Range(0, 10)]
		public int vibrato;
		[Range(0, 90)]
		public float randomness;
    }

    public enum CameraShakePreset
    {
        Mild,
        Medium,
        Strong
    }
}