using UnityEngine;
using UniRx;

namespace QuantumArk
{
	public class OneWayPlatform : MonoBehaviour
	{
		private PlatformEffector2D effector2D;

		private PlayerInput playerInput;
		private PlayerControl playerControl;

		// later replaced with DI
		private void Start()
		{
			var player = GameObject.FindGameObjectWithTag("Player");
			if (player == null) return;

			playerInput = player.GetComponent<PlayerInput>();
			playerControl = player.GetComponent<PlayerControl>();

			playerInput.OnCrouchPressed().TakeUntilDisable(this).Subscribe(_ => SetGoThruFromAbove());
			playerInput.OnJumpPressed().TakeUntilDisable(this).Subscribe(_ => SetGoThruFromBelow());
		}

		private void OnEnable()
		{
			effector2D = GetComponent<PlatformEffector2D>();
		}

		private void SetGoThruFromBelow()
		{
			effector2D.rotationalOffset = 0f;
		}

		private void SetGoThruFromAbove()
		{
			if (!playerControl.IsGrounded()) return;
			effector2D.rotationalOffset = 180f;
		}
	}
}
