

namespace QuantumArk
{
	public abstract class EnemyFSM : MonoBehaviourFSM
	{
		public EnemyControl control;
		public EnemyView view;
		public EnemyAI ai;

		public abstract void Init();
	}
}
