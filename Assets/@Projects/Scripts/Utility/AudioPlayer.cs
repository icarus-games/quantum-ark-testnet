using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace QuantumArk
{
	public class AudioPlayer : MonoBehaviourSingleton<AudioPlayer>
	{
		private AudioSource source;

		public AudioItem[] clips;

		protected override void Awake()
		{
			base.Awake();
			source = GetComponent<AudioSource>();
		}

		public void PlaySfx(string sfxName)
		{
			AudioItem audio = clips.FirstOrDefault(i => i.sfxName.Equals(sfxName));
			if (audio == null)
			{
				Debug.Log($"SFX with {sfxName} is not found.");
				return;
			}
			source.PlayOneShot(audio.clip);
		}

		public float GetDuration(string sfxName)
		{
			AudioItem audio = clips.FirstOrDefault(i => i.sfxName.Equals(sfxName));
			if (audio == null)
			{
				Debug.Log($"SFX with {sfxName} is not found.");
				return 0f;
			}
			return audio.clip.length;
		}
	}

	[System.Serializable]
	public class AudioItem
	{
		public string sfxName;
		public AudioClip clip;
	}
}
