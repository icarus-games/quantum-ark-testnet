using System.Collections.Generic;
using System.Linq;
using QuantumArk.Blockchain;
using UniRx;
using UnityEngine;

namespace QuantumArk.UI
{
    public class InventoryMenu : Menu<InventoryMenu>
    {
        [SerializeField] private InventoryView view = null;        
        private PostGameViewState currentState;

        public override void Initialize()
        {
            InitItems();
        }

        private async void InitItems()
        {
            Blackboard.Instance.newlyMintedItems = new List<string>();
            LoadingMenu.Open();
            view.Initialize(Blackboard.Instance);
            view.actionButton.OnClickAsObservable().Subscribe(_ => ToggleState());
            view.homeButton.OnClickAsObservable().TakeUntilDisable(this).Subscribe(_ => OnBackPressed());
            
            await Blackboard.Instance.FetchLatestData();
            
            ChangeState(PostGameViewState.Equip);
            LoadingMenu.Close();
            
        }

        private void ChangeState(PostGameViewState state)
        {
            currentState = state;
            view.ChangeViewState(state);

            switch (state)
            {
                case PostGameViewState.Equip:
                    view.equipView.OnItemClicked().TakeUntilDisable(view.equipView).Subscribe(OnItemClicked);
                    break;
                case PostGameViewState.Fuse:
                    view.fuseView.fuseButton.OnClickAsObservable().TakeUntilDisable(view.fuseView).Subscribe(OnFuseClicked);
                    break;
            }

        }

        private void ToggleState()
        {            
            ChangeState(currentState == PostGameViewState.Fuse ? PostGameViewState.Equip : PostGameViewState.Fuse);
        }

        private void OnItemClicked(CosmeticItem item)
        {
            Blackboard.Instance.EquippedItem = item;
            view.equipView.Refresh();
        }

        private async void OnFuseClicked(Unit obj)
        {
            var materials = view.fuseView.selectedMaterials;
            if (materials.Count != 2) return;

            var matA = materials[0];
            var matB = materials[1];
            
            // if equipped item is used as material, change the equipped Item to a random item after fusing done
            var equippedItem = Blackboard.Instance.EquippedItem.nftId;
            var isEquippedItemUsedAsMaterial = matA.nftId == equippedItem || matB.nftId == equippedItem;

            LoadingMenu.Open();
            var bcResult = await BlockchainManager.Instance.Fuse(matA.nftId, matB.nftId, FuseTable.GetFuseResult(matA, matB));

            if (bcResult.error != null)
            {
                // THROW TOAST HERE
                LoadingMenu.Close();

            }
            else
            {
                var fuseResult = bcResult.result;
                Blackboard.Instance.newlyMintedItems.Add(fuseResult.Replace("\"", ""));
            
                Debug.Log($"Fusing complete with : {bcResult}");
                await Blackboard.Instance.FetchLatestData();

                if (isEquippedItemUsedAsMaterial)
                {
                    Debug.Log("randomized equipped item");
                    Blackboard.Instance.EquippedItem = Blackboard.Instance.ownedItems[Random.Range(0, Blackboard.Instance.ownedItems.Count)];
                }
                LoadingMenu.Close();

                ChangeState(PostGameViewState.Equip);
                view.equipView.FlagNewItems();

                ResultMenu.Open("Fuse Complete, You Got", Blackboard.Instance.ownedItems.Last(), string.Empty);
            }
        }

        public override void OnBackPressed()
        {
            TitleScreenMenu.Open();
        }
    }
}