using Lean.Pool;
using System;
using UnityEngine;

namespace QuantumArk
{
    public class Weapon : MonoBehaviour
    {
        [SerializeField] private float fireRate = 0.1f;
        [SerializeField] private float damage = 0;
        [SerializeField] private BulletSpawner[] bulletSpawners = null;
        [SerializeField] private Sprite sprite = null;
        [SerializeField] private float angleVariation = 0;
        [SerializeField] private Transform muzzleFxParent = null;

		private float lastShotTime = float.MinValue;

		public Action OnShoot;
		public string shotSfx = "";
		public string shotVfx = "";
		public string weaponName = "";

		private void Awake()
        {
            if (bulletSpawners == null || bulletSpawners.Length == 0)
            {
                bulletSpawners = GetComponentsInChildren<BulletSpawner>();
            }
        }

        public void TryShoot()
        {
            if (Time.time - lastShotTime >= fireRate)
            {
                Shoot();
                lastShotTime = Time.time;
            }
        }

        private void Shoot()
        {
            for (int i = 0; i < bulletSpawners.Length; i++)
            {
                var extraAngle = angleVariation == 0f ? 0f : UnityEngine.Random.Range(-angleVariation, angleVariation);
                bulletSpawners[i].Spawn(damage, extraAngle);
            }
			OnShoot?.Invoke();
			VFXPlayer.Instance.SpawnVFX(shotVfx, muzzleFxParent);
		}
    }
}