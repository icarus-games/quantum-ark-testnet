using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class FlyingStalkerState_Die : EnemyBaseState
	{
		public FlyingStalkerState_Die(FlyingStalkerFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			view.PlayDeath();
		}
	}
}
