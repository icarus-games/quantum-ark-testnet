using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace QuantumArk.Blockchain
{
    public class BlockchainAPI : MonoBehaviourSingleton<BlockchainAPI>, IBlockchainApi
    {
        public string serverUrl;
        public string ownerId;

        public List<BC_TokenResult> items;

        private IBlockchainApi api;

        protected override void Awake()
        {
            #if UNITY_EDITOR
            api = gameObject.AddComponent<EditorBlockchainApi>();
            #elif UNITY_WEBGL

            #endif
        }

        public void ConnectWallet(Action callback) => api.ConnectWallet(callback);
        public bool IsWalletConnected => api.IsWalletConnected;
        public string WalletAddress => api.WalletAddress;

        public void FetchOwnedItems(Action<List<BC_TokenResult>> callback)
        {
            var requestUrl = $"{serverUrl}/token?owner_id={ownerId}&__skip=0&__limit=10";
            
            GenerateRequest(requestUrl,
                 new List<Tuple<string, string>>() {new Tuple<string, string>("Authorization", $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes("admin:admin"))}")},
                 resultText =>
            {
                if (string.IsNullOrEmpty(resultText))
                {
                    callback?.Invoke(null);
                    return;
                }
                
                items = new List<BC_TokenResult>();
                var results = JObject.Parse(resultText)["data"]["results"].Children().ToList();
                foreach (var result in results)
                {
                    var item = result.ToObject<BC_TokenResult>();
                    items.Add(item);
                }
                
                callback.Invoke(items);
            });
        }

        public void GenerateRequest(string uri, List<Tuple<string, string>> headers, Action<string> resultCallback)
        {
            StartCoroutine(_GenerateRequest());
            IEnumerator _GenerateRequest()
            {
                using (UnityWebRequest request = UnityWebRequest.Get(uri))
                {
                    if (headers != null)
                    {
                        for (int i = 0; i < headers.Count; i++)
                        {
                            request.SetRequestHeader(headers[i].Item1, headers[i].Item2);
                        }
                    }
                    // request.SetRequestHeader("Authorization", "Basic YWRtaW46YWRtaW4=");
                    yield return request.SendWebRequest();

                    Debug.Log($"{request.responseCode} : {request.error}");
                    var result = string.Empty;
                    if (string.IsNullOrEmpty(request.error))
                    {
                        result = request.downloadHandler.text;
                        
                        Debug.Log(request.downloadHandler.text);
                    }
                    
                    resultCallback?.Invoke(result);
                }
            }
        }
    }
}