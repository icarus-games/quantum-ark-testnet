using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace QuantumArk
{
    public class RainOfBulletAbility : MonoBehaviour
    {
        [SerializeField] private Transform bulletSpawnerParent = null;
        [SerializeField] private BulletSpawner bulletSpawner = null;
        [SerializeField] private float damage = 0f;
        [SerializeField] private float duration = 5f;
        [SerializeField] private float bulletDelay = 0.2f;
        [SerializeField] private float area = 0f;

        private Transform followObject = null;
        
        public void ActivateAbility(Transform followObject)
        {
            this.followObject = followObject;
            
            StartCoroutine(_ActivateAbility());
            IEnumerator _ActivateAbility()
            {
                var shootAmount = Mathf.FloorToInt(duration / bulletDelay);
                var waitDelay = new WaitForSeconds(bulletDelay);

                UpdateSpawnerXPos(bulletSpawnerParent.position.x);
                
                for (int i = 0; i < shootAmount; i++)
                {
                    var newPos = bulletSpawnerParent.position.x + Random.Range(-area, area);
                    UpdateSpawnerXPos(newPos);
                    
                    bulletSpawner.Spawn(damage, 0f);
                    yield return waitDelay;
                }

                this.followObject = null;
            }
        }

        private void Update()
        {
            if (followObject != null)
            {
                this.transform.position = followObject.transform.position;
            }
        }

        private void UpdateSpawnerXPos(float posX)
        {
            var _transform = bulletSpawner.transform;
            
            var pos = _transform.position;
            pos.x = posX;
            _transform.position = pos;
        }

		public BulletSpawner BulletSpawner => this.bulletSpawner;
    }
}