using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public class PlayerFSM : MonoBehaviourFSM
	{
		public PlayerConfig config;

		public PlayerControl control;
		public PlayerView view;
		public PlayerInput input;

		private void Start()
		{
			control.SetPlayerConfig(config);

			ChangeState(new PlayerState_Idle(this));
		}
	}
}