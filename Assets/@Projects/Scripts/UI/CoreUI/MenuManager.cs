﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace QuantumArk.UI
{
    public class MenuManager : MonoBehaviourSingleton<MenuManager>
    {
        public List<Menu> stack = new List<Menu>();

        private Dictionary<string, Menu> pool = new Dictionary<string, Menu>();
        private Dictionary<PlacementType, Transform> parents = new Dictionary<PlacementType, Transform>();

        #region events
        public delegate void MenuShowHandler(Menu menu);
        public static event MenuShowHandler onShowE;
        public static event MenuShowHandler onHideE;
        public static void BroadcastMenuShow(Menu menu) { onShowE?.Invoke(menu); }
        public static void BroadcastMenuHide(Menu menu) { onHideE?.Invoke(menu); }
        #endregion


        public T Open<T>() where T : Menu
        {
            T menu = GetInstance<T>();
            AddToStack(menu);
            Show(menu);

            return menu;
        }

        public void Close<T>() where T : Menu
        {
            var type = typeof(T).Name;

            if (!IsMenuActive<T>())
            {
                Debug.LogErrorFormat("{0} is not in instances.", type);
                return;
            }

            if (stack.Count == 0)
            {
                Debug.LogErrorFormat("{0} cannot be closed because menu stack is empty", type);
                return;
            }

            var menu = pool[type];

            RemoveFromStack(menu);
            Hide(menu);
        }

        private bool IsMenuActive<T>() where T : Menu
        {
            return pool.ContainsKey(typeof(T).Name);
        }

        public bool IsMenuShown<T>() where T : Menu
        {
            if (!IsMenuActive<T>() || stack.Count == 0) return false;

            var menu = pool[typeof(T).Name];
            return stack.Last() == menu;
        }

        private void ClearStack()
        {
            while (stack.Count > 0)
            {
                var menu = stack.Last();
                Hide(menu);
                stack.Remove(menu);
            }
        }

        private void AddToStack(Menu menu)
        {
            if (menu.menuType == MenuType.Panel)
            {
                ClearStack();
            }

            stack.Add(menu);
        }

        private void RemoveFromStack(Menu menu)
        {
            var lastItem = stack.Last();
            stack.Remove(menu);

            if (lastItem == menu && stack.Count > 0)
            {
                stack.Last().Show();
            }
        }

        private void Show(Menu menu)
        {
            menu.Initialize();
            menu.Show();            
            menu.transform.SetAsLastSibling();
        }

        private void Hide(Menu menu)
        {
            // remove from instances dictionary if the item is destroyed
            if (menu.hideBehaviour == HideBehaviour.DestroyWhenClosed)
            {
                pool.Remove(menu.GetType().Name);
            }

            menu.Hide();
        }

        private T GetInstance<T>() where T : Menu
        {
            string type = typeof(T).Name;
            T menu = null;

            if (pool.ContainsKey(type))
            {
                return (T)pool[type];
            }
            else
            {
                var prefab = Resources.Load<T>(typeof(T).Name);

                if (prefab != null)
                {
                    menu = Instantiate(prefab, GetParent(prefab.placementType));
                }
                else
                {
                    throw new MissingReferenceException("Prefab not found for type " + typeof(T));
                }
                
                pool.Add(type, menu);
                return menu;
            }
        }
        
        private Transform GetParent(PlacementType placement)
        { 
            if (!parents.ContainsKey(placement)) 
            {
                var _parent = transform.Find($"PARENTS/{placement}");
                parents.Add(placement, _parent);
            }

            return parents[placement];
        }

        private void Update()
        {
            // On Android the back button is sent as Esc
            if (Input.GetKeyDown(KeyCode.Escape) && stack.Count > 0)
            {
                stack.Last().OnBackPressed();
            }
        }
    }
}
