﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace QuantumArk
{
    [RequireComponent(typeof(Button))]
    public class ButtonExtension : MonoBehaviour, IPointerClickHandler
    {
        public Button button;
        public string clickedSfxName = "buttonClick";

        private void Awake()
        {
            button = GetComponent<Button>();
        }

        private void PlaySFX()
        {
            if (!string.IsNullOrEmpty(clickedSfxName))
            {
                AudioPlayer.Instance.PlaySfx(clickedSfxName);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            PlaySFX();
        }
    }
}
