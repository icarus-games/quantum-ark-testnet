using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using QuantumArk;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace QuantumArk.UI
{
    public class EquipView : MonoBehaviour
    {
        [SerializeField] private Transform itemParent = null;
        [SerializeField] private CosmeticItemView itemPrefab = null;
        [SerializeField] private Image characterImage = null;
        
        private Subject<CosmeticItem> onItemClicked;
        private List<CosmeticItemView> items = new List<CosmeticItemView>();
        
        public Subject<CosmeticItem> OnItemClicked() => onItemClicked ?? (onItemClicked = new Subject<CosmeticItem>());
        
        private bool isInitialized = false;
        private Blackboard blackboard;
        
        public void Initialize(Blackboard blackboard)
        {
            this.blackboard = blackboard;
            StartCoroutine(UpdateInventoryItems());
        }

        public void Refresh()
        {
            UpdateEquippedItem();
        }

        private IEnumerator UpdateInventoryItems()
        {
            var itemData = Blackboard.Instance.ownedItems;

            for (int i = items.Count - 1; i >= 0; i--)
            {
                Destroy(items[i].gameObject);
            }

            yield return new WaitForEndOfFrame();
            items = new List<CosmeticItemView>();

            for (int i = 0; i < itemData.Count; i++)
            {
                var cosmeticItem = itemData[i];
                var item = Instantiate(itemPrefab, itemParent);
                item.Initialize(cosmeticItem);
                item.actionButton.OnClickAsObservable().TakeUntilDestroy(item).Subscribe(_ => { onItemClicked?.OnNext(cosmeticItem); });
                
                items.Add(item);
            }
            
            UpdateEquippedItem();
            FlagNewItems();
        }

        private void UpdateEquippedItem()
        {
            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];

                var isEquippedItem = item.cosmeticItem == blackboard.EquippedItem;
                
                item.actionButton.interactable = !isEquippedItem;
                item.actionButtonText.text = isEquippedItem ? "SELECTED" : "EQUIP";
            }
            
            characterImage.sprite = Resources.Load<Sprite>($"CharacterStatic/{blackboard.EquippedItem.AssetId}");
            characterImage.DOFade(0f, 0.001f);
            characterImage.DOFade(1f, 0.3f);
        }

        public void FlagNewItems()
        {
            var flaggedItems = items.Where(x => blackboard.newlyMintedItems.Any(y => x.cosmeticItem.nftId == y)).ToList();

            for (int i = 0; i < flaggedItems.Count; i++)
            {
                flaggedItems[i].FlagNewItem();
            }
        }
    }
}