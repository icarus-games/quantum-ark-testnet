
burstGroundShooter.png
size: 1024,256
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: false
  xy: 250, 61
  size: 157, 189
  orig: 157, 189
  offset: 0, 0
  index: -1
hand/Burst
  rotate: true
  xy: 166, 39
  size: 211, 84
  orig: 211, 84
  offset: 0, 0
  index: -1
hand/Color
  rotate: false
  xy: 639, 130
  size: 89, 120
  orig: 89, 120
  offset: 0, 0
  index: -1
hand/Light
  rotate: true
  xy: 407, 0
  size: 79, 78
  orig: 84, 86
  offset: 2, 4
  index: -1
hand/Line
  rotate: true
  xy: 543, 20
  size: 91, 123
  orig: 91, 123
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 0, 8
  size: 242, 166
  orig: 248, 166
  offset: 0, 0
  index: -1
leg
  rotate: false
  xy: 543, 111
  size: 96, 139
  orig: 97, 158
  offset: 1, 19
  index: -1
wheel
  rotate: true
  xy: 407, 79
  size: 171, 136
  orig: 171, 136
  offset: 0, 0
  index: -1
