using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class EnemyControl : MonoBehaviour
	{
		public Movement movement;
		public GroundCheck groundCheck;
		public DamageTarget damageTarget;
		public EnemyView view;
		public CapsuleCollider2D dmgReceiverCollider;

		private void OnEnable()
		{
			dmgReceiverCollider.enabled = true;
			damageTarget.OnDamaged = delegate { view.PlayDamaged(damageTarget.OtherPos); };
		}

		public void MoveTowards(Vector3 target) => movement.MoveTowards(transform.position, target);

		public void MoveTowardsOnAir(Vector3 target) => movement.MoveTowardsOnAir(transform.position, target);

		public bool IsGrounded() => groundCheck.CheckGrounded();

		public void OnEnemyDeath(Action act) => damageTarget.OnHealthZero = delegate { act?.Invoke(); };

		public void Dead()
		{
			dmgReceiverCollider.enabled = false;
		}
	}
}
