using System;
using UniRx;

namespace QuantumArk
{
	public class PlayerState_Dash : BasePlayerState
	{
		public PlayerState_Dash(PlayerFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			control.SetIsDashCooldown(true);
			control.StartDashCooldown();
			Observable.EveryFixedUpdate().Subscribe(_ => control.Dash()).AddTo(disposable);
			Observable.Timer(TimeSpan.FromSeconds(fsm.config.DashDuration)).Subscribe(_ => CheckOnGrounded()).AddTo(disposable);
			
			input.OnChangeWeaponPressed().Subscribe(_ => control.SwitchWeapon()).AddTo(disposable);
			view.SetDashVFX(true);
		}

		public override void OnExit()
		{
			base.OnExit();
			view.SetDashVFX(false, 0.3f);
		}

		private void CheckOnGrounded()
		{
			if (control.IsGrounded())
			{
				view.PlayLanding();
				ChangeStateToIdle();
			}
			else
			{
				fsm.ChangeState(new PlayerState_Jump(fsm));
				view.PlayOnAir();
			}
		}
	}
}
