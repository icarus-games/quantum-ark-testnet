using System;
using UniRx;
using UnityEngine;

namespace QuantumArk.UI
{
    public class ResultMenu : Menu<ResultMenu>
    {
        [SerializeField] private ResultView view = null;

        private static string title;
        private static CosmeticItem cosmeticItem;
        private static string coinAmount;
        private static Action onNextClicked;

        public static void Open(string _title, CosmeticItem _cosmeticItem, string _coinAmount, Action _onNextClicked = null)
        {
            title = _title;
            cosmeticItem = _cosmeticItem;
            coinAmount = _coinAmount;
            onNextClicked = _onNextClicked;
            
            Open();
        }

        public override void Initialize()
        {
            view.Initialize(title, cosmeticItem, coinAmount);

            view.nextButton.OnClickAsObservable().TakeUntilDisable(this).Subscribe(_ =>
            {
                if (onNextClicked == null)
                {
                    OnBackPressed();
                }
                else
                {
                    onNextClicked?.Invoke();
                }
            });
        }
    }
}