﻿using System;
using UnityEngine;

namespace QuantumArk.UI
{
    public class Menu<T> : Menu where T : Menu<T>
    {
        public static T Instance { get; set; }

        public static event Action<T> onShow;
        public static event Action<T> onHide;

        protected virtual void Awake()
        {
            Instance = (T)this;
        }

        protected virtual void OnDestroy()
        {
            Instance = null;
        }

        public static T Open()
        {
            T menu = MenuManager.Instance.Open<T>();
            return menu;
        }

        public static void Close()
        {
            MenuManager.Instance.Close<T>();
        }

        public override void Initialize()
        {
        }

        public override void OnBackPressed()
        {
            Close();
        }

        public override void OnShow()
        {
            onShow?.Invoke((T) this);
        }

        public override void OnHide()
        {
            onHide?.Invoke((T)this);
        }
    }

    public abstract class Menu : MonoBehaviour
    {
        public MenuType menuType = MenuType.Panel;
        public HideBehaviour hideBehaviour = HideBehaviour.DestroyWhenClosed;
        public PlacementType placementType = PlacementType.Default;
        
        public int priority = 0;

        #if UNITY_EDITOR
        private bool IsPopUpPanel()
        {
            return menuType == MenuType.Popup;
        } 
        #endif

        public string ID { get { return gameObject.name.Replace("(Clone)", ""); } }

        public abstract void Initialize();
        public abstract void OnBackPressed();
        public abstract void OnShow();
        public abstract void OnHide();

        public void Show()
        {
            DisplayAndBroadcast();
        }

        public void Hide()
        {
            Display(false);
            OnHide();
            MenuManager.BroadcastMenuHide(this);
        }

        public virtual void DisplayAndBroadcast()
        {
            Display(true);
            OnShow();
            MenuManager.BroadcastMenuShow(this);
        }

        protected void Display(bool isActive)
        {
            if (isActive)
            {
                switch (hideBehaviour)
                {
                    case HideBehaviour.DisableGameObject:
                    case HideBehaviour.DestroyWhenClosed:
                        gameObject.SetActive(true);
                        break;
                }
            }
            else
            {
                switch (hideBehaviour)
                {
                    case HideBehaviour.DisableGameObject:
                        gameObject.SetActive(false);
                        break;

                    case HideBehaviour.DestroyWhenClosed:
                        Destroy(gameObject);
                        break;
                }
            }
        }
    }

    public enum HideBehaviour
    {
        DestroyWhenClosed,
        DisableGameObject
    }

    public enum MenuType
    {
        Panel,
        Popup
    }

    public enum PlacementType
    {
        Background = -10,
        Default = 0,
        Overlay = 10,
        Popup = 20,
        TopOrder = 25,
        Tutorial = 30
    }
}
