using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class DamageTargetView : MonoBehaviour
	{
		public string[] sfxOnDamagedKey;

		public void PlayOnDamaged()
		{
			string sfxKey = "";
			if (sfxOnDamagedKey.Length == 0) return;
			sfxKey = sfxOnDamagedKey[Random.Range(0, sfxOnDamagedKey.Length)];
			AudioPlayer.Instance.PlaySfx(sfxKey);
		}
	}
}
