using UnityEngine;
using UnityEngine.UI;

namespace QuantumArk.UI
{
    public class LoadoutView : MonoBehaviour
    {
        public Button startButton = null;
        public Button homeButton = null;

        [SerializeField] private Image characterImage = null;
        
        public void SetEquippedItem(CosmeticItem cosmeticItem)
        {
            characterImage.sprite = Resources.Load<Sprite>($"LoadoutCharacter/{cosmeticItem.AssetId}");
        }
    }
}