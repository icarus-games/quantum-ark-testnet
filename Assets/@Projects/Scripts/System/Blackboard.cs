using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuantumArk.Blockchain;
using UniRx;

namespace QuantumArk
{
    public class Blackboard : MonoBehaviourSingleton<Blackboard>
    {
        public List<CosmeticItem> ownedItems = new List<CosmeticItem>();
        public List<string> newlyMintedItems = new List<string>();
        public StringReactiveProperty coins = new StringReactiveProperty("");

        private CosmeticItem equippedItem;
        private AppState appState;

        public void Construct(AppState appState)
        {
            equippedItem = CosmeticItem.MockItem();
            this.appState = appState;
        }

        public async Task FetchLatestData()
        {
            await RefreshItem();
            await RefreshCoins();
        }

        private async Task RefreshItem()
        {
            var bcResult = await BlockchainManager.Instance.GetOwnedNFT();
            var resultList = bcResult.result;
            if (bcResult.error != null) return;
            
            ownedItems = new List<CosmeticItem>();
            for (int i = 0; i < resultList.Count; i++)
            {
                ownedItems.Add(new CosmeticItem(resultList[i]));
            }
            
            // (hotfix) if there's no owned item from the appstate, reset it. Then use the first item, available
            var ownedFromAppState = ownedItems.SingleOrDefault(x => x.nftId == appState.equippedItem.Value);
            if (default(CosmeticItem) == ownedFromAppState)
            {
                appState.equippedItem.Value = ownedItems[0].nftId;
            }
            
            equippedItem = GetItemFromNftId(appState.equippedItem.Value);
        }

        private async Task RefreshCoins()
        {
            var bcResult = await BlockchainManager.Instance.GetAccountBalance();
            if (bcResult.error != null) return;

            coins.Value = bcResult.result;
        }

        public CosmeticItem EquippedItem
        {
            get => equippedItem;
            set
            {
                equippedItem = value;
                appState.equippedItem.Value = value.nftId;
            }
        }

        private CosmeticItem GetItemFromNftId(string nftId) => ownedItems.SingleOrDefault(x => x.nftId == nftId);
    }
}