using UnityEngine;
using UnityEngine.UI;

namespace QuantumArk.UI
{
	public class GameCompleteView : MonoBehaviour
	{
		public Button restartButton;
		public CanvasGroup canvasGroup;

		public void Show() => canvasGroup.alpha = 1;

		public void Hide() => canvasGroup.alpha = 0;
	}
}
