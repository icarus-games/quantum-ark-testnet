using QuantumArk;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace QuantumArk.UI
{
    public class GameHudView : MonoBehaviour
    {
        [SerializeField] private Image healthBar = null;
        [SerializeField] private Image specialAttackBar = null;
        [SerializeField] private TextMeshProUGUI specialAttackText = null;
        [SerializeField] private Image activeWeaponSprite = null;

        [SerializeField] private Sprite[] weaponSpriteSources = null;
        

        private PlayerControl player;

        public void Initialize(PlayerControl playerControl)
        {
            this.player = playerControl;
            
            player.Health.TakeUntilDisable(this).Subscribe(OnHealthUpdated);
            player.SpecialAttackTime.TakeUntilDisable(this).Subscribe(OnSpecialAttackTimeUpdated);
            player.ActiveWeaponIndex.TakeUntilDisable(this).Subscribe(OnActiveWeaponChanged);
        }

        private void OnHealthUpdated(float value)
        {
            healthBar.fillAmount = player.Health.Value / player.MaxHealth;
        }

        private void OnSpecialAttackTimeUpdated(float value)
        {
            specialAttackBar.fillAmount = player.SpecialAttackProgress;
            specialAttackText.text = $"{(player.SpecialAttackProgress * 100):0}%";
        }

        private void OnActiveWeaponChanged(int weaponIndex)
        {
            activeWeaponSprite.sprite = weaponSpriteSources[weaponIndex];
        }
    }
}