using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class GameStateFSM : MonoBehaviourFSM
	{
		//Inject every Gameplay elements here
		//This MonoBehaviour class works as first door of the gameplay

		private void Start()
		{
			ChangeState(new GameState_Start(this));
		}
	}
}
