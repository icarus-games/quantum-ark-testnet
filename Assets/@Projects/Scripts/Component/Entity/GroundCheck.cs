using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class GroundCheck : MonoBehaviour
	{
		[SerializeField] private Transform groundCheck = null;
		[SerializeField] private LayerMask groundLayer;

		[SerializeField] private float groundRadius = 0.2f;
		[SerializeField] private bool showGizmos = false;

		private bool isGrounded;

		public bool CheckGrounded()
		{
			isGrounded = false;
			Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundRadius, groundLayer);
			foreach (var col in colliders)
			{
				if (col.gameObject != gameObject)
				{
					isGrounded = true;
				}
			}
			return isGrounded;
		}

		private void OnDrawGizmosSelected()
		{
			if (groundCheck == null) return;
			if (!showGizmos) return;
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(groundCheck.position, groundRadius);
		}
	}
}