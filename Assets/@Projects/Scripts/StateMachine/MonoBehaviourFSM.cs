using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	[System.Serializable]
	public class MonoBehaviourFSM : MonoBehaviour
	{
		public bool isLogged = false;

		private IState activeState;
		private IState previousState;

		private void OnDestroy()
		{
			ClearState();
		}

		public void ChangeState(IState newState)
		{
			ExitActiveState();
			EnterNewState(newState);
		}

		public void ClearState()
		{
			ExitActiveState();
			EnterNewState(null);
		}

		private void ExitActiveState()
		{
			if (activeState != null)
			{
				activeState.OnExit();
				if (isLogged) Debug.Log($"{name} FSM exit <color=magenta>{activeState}</color>");
			}
		}

		private void EnterNewState(IState state)
		{
			previousState = activeState;
			activeState = state;
			if (isLogged) Debug.Log($"{name} FSM enter <color=cyan>{activeState}</color>");
			if (activeState != null) activeState.OnEnter();
		}

		public virtual void Initialize()
		{

		}

		private void Update()
		{
			if (activeState != null) activeState.OnUpdate();
		}
	}
}
