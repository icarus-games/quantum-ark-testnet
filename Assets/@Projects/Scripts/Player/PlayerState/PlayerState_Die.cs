using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class PlayerState_Die : BasePlayerState
	{
		public PlayerState_Die(PlayerFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			control.ResetVelocity();
			view.PlayDeath();
			GameEvents.OnPlayerDead();
		}
	}
}
