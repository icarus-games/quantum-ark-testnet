using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class GameState_Start : BaseGameState
	{
		public GameState_Start(GameStateFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			GameEvents.OnGameStart();
			fsm.ChangeState(new GameState_Running(fsm));
		}
	}
}
