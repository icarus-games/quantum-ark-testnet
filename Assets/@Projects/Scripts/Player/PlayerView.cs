using System;
using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
    public class PlayerView : MonoBehaviour
    {
        [SerializeField] private SkeletonAnimation skeletonAnimation = null;
        [SerializeField] private MuzzleAimCrosshair muzzleAimCrosshair = null;
        [SerializeField] private PlayerSkinView skinView = null;

		[Header("Embedded VFX")]
		[SerializeField] private ParticleSystem dashVfx;
		[SerializeField] private Transform centerCollider;

		[Header("Shader VFX")]
		[SerializeField] private Animator animator;
		[SerializeField] private string onDamagedTrigger;

		private const string STEP_EVENT_NAME = "step";

		private AudioPlayer audioPlayer => AudioPlayer.Instance;

        private int faceDirection;
		private int moveDirection;
		private bool isMoving; 

        private void OnEnable()
        {
			SetDashVFX(false);
            muzzleAimCrosshair.facingSign.TakeUntilDisable(this).Subscribe(UpdateFacing);
			skeletonAnimation.AnimationState.Event += HandleEvent;
		}

		private void HandleEvent(TrackEntry trackEntry, Spine.Event e)
		{
			if (e.Data.Name == STEP_EVENT_NAME)
			{
				bool rand = UnityEngine.Random.Range(0, 2) == 0;
				string stepSfx = rand ? "step1" : "step2";
				audioPlayer.PlaySfx(stepSfx);
			}
		}

		public void SetWeaponSkin(string skinName)
		{
			skinView.ChangeWeaponSkin(skinName);
		}

		public void PlayIdle(bool isShooting)
        {
			var anim = isShooting ? "idle_mixShoot" : "idle";
			skeletonAnimation.AnimationState.SetAnimation(0, anim, true);
        }

        public void PlayRun()
		{
			string animName = faceDirection == moveDirection ? "running" : "stepBack";
            skeletonAnimation.AnimationState.SetAnimation(0, $"{animName}_mixShoot", true);
		}

		public void UpdateRunDirection(float dir)
		{
			if (dir == 0) return;
			if (moveDirection != Mathf.FloorToInt(dir))
			{
				moveDirection = Mathf.FloorToInt(dir);
				PlayRun();
			}
		}

		public void SetIsMoving(bool val) => isMoving = val;

		public void PlayJump()
		{
			skeletonAnimation.AnimationState.SetAnimation(0, "jump", false);
			skeletonAnimation.AnimationState.AddAnimation(0, "jump_onAir", false, 0);
			audioPlayer.PlaySfx("jump");
			VFXPlayer.Instance.SpawnVFX("jump", transform.position, transform);
		}

		public void PlayOnAir()
		{
			skeletonAnimation.AnimationState.SetAnimation(0, "jump_onAir", false);
		}

		public void PlayDash()
		{
			skeletonAnimation.AnimationState.SetAnimation(0, "dash", false);
			audioPlayer.PlaySfx("dash");
		}

		public void SetDashVFX(bool isEnabled, float delay = 0f)
		{
			StartCoroutine(SetDashVFXCoroutine(isEnabled, delay));
		}

		private IEnumerator SetDashVFXCoroutine(bool isEnabled, float delay)
		{
			yield return new WaitForSeconds(delay);
			dashVfx.gameObject.SetActive(isEnabled);
		}

		public void PlayLanding()
		{
			audioPlayer.PlaySfx("landing");
			VFXPlayer.Instance.SpawnVFX("landing", transform.position, transform);
		}

		public void PlayUltimate()
		{
			skeletonAnimation.AnimationState.SetAnimation(0, "ultimate", false);
		}

		public void PlayDeath()
		{
			skeletonAnimation.AnimationState.SetAnimation(0, "idle", false);
		}

		public void PlayDamaged(Vector3 sourcePos)
		{
			int idx = UnityEngine.Random.Range(1, 4);
			string dmgSfx = $"damaged_{idx}";
			audioPlayer.PlaySfx(dmgSfx);
			var angle = MathUtility.FindAngleBetweenTwoPoints(sourcePos, centerCollider.position);
			VFXPlayer.Instance.SpawnVFX("playerDamaged", sourcePos, transform, angle);
			CameraFx.Instance.Shake(CameraShakePreset.Medium);

			animator.SetTrigger(onDamagedTrigger);
		}

        public void PlayShoot(bool isShooting)
        {
	        if(isShooting) skeletonAnimation.AnimationState.SetAnimation(1, "shooting", true);
	        else skeletonAnimation.state.AddEmptyAnimation(1, 0.5f, 0.1f);
		}

        private void UpdateFacing(int facingSign)
        {
            var scaleX = Mathf.CeilToInt(skeletonAnimation.Skeleton.ScaleX);
			faceDirection = facingSign;
			dashVfx.transform.localScale = new Vector3(faceDirection, 1, 1);
			if (scaleX != facingSign)
            {
                skeletonAnimation.Skeleton.ScaleX = facingSign;
				if (isMoving) PlayRun();
			}
		}
    }
}