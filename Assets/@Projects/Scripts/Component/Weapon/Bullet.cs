using System;
using UnityEngine;

namespace QuantumArk
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rb = null;
        [SerializeField] private float launchSpd = 10f;
        [SerializeField] private DamageSource damageSource = null;

        private void Awake()
        {
            if (damageSource == null) damageSource = GetComponent<DamageSource>();
        }

        private void OnDisable()
        {
            Stop();
        }

        public void Launch(float angle)
        {
            rb.AddForce(MathUtility.DegreeToVector2(angle) * launchSpd);
        }

        public void Stop()
        {
            rb.velocity = Vector3.zero;
        }

        public void SetDamage(float damagePts)
        {
            damageSource.damagePts = damagePts;
        }
    }
}
