using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public abstract class BaseGameState : IState
	{
		protected GameStateFSM fsm;
		protected CompositeDisposable disposable;

		public BaseGameState(GameStateFSM fsm)
		{
			this.fsm = fsm;
			disposable = new CompositeDisposable();
		}

		~BaseGameState()
		{
		}

		public abstract void OnEnter();

		public virtual void OnExit()
		{
			if (!disposable.IsDisposed) disposable.Dispose();
		}

		public virtual void OnUpdate()
		{

		}
	}
}
