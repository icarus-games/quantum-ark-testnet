using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class ParallaxBackground : MonoBehaviour
	{
		private float length;
		private float startPos;
		public Transform target;
		public float parallaxSpeed;

		private void Start()
		{
			startPos = transform.position.x;
			length = GetComponent<SpriteRenderer>().bounds.size.x;
		}

		private void Update()
		{
			float temp = target.transform.position.x * (1 - parallaxSpeed);
			float distance = target.transform.position.x * parallaxSpeed;
			transform.position = new Vector3(startPos + distance, transform.position.y, transform.position.z);

			if (temp > startPos + length) startPos += length;
			else if (temp < startPos - length) startPos -= length;
		}
	}
}
