using System;
using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using Spine.Unity.AttachmentTools;
using UnityEngine;

namespace QuantumArk
{
	public class PlayerSkinView : MonoBehaviour
	{
		private SkeletonAnimation skeletonAnimation;
		private Skin characterSkin;
		private Material runtimeMaterial;
		private Texture2D runtimeAtlas;
		
		[SpineSkin] public string baseSkin = "default";
		[SpineSkin] public string baseWeaponSkin = "rifle";
		public string selectedJacketSkinName = "";
		public string selectedWeaponSkinName = "";

		private void Awake()
		{
			skeletonAnimation = GetComponent<SkeletonAnimation>();
		}

		private void Start()
		{
			selectedWeaponSkinName = baseWeaponSkin;
			Debug.Log(Blackboard.Instance.EquippedItem.AssetId);
			selectedJacketSkinName = Blackboard.Instance.EquippedItem.AssetId;
			UpdateCharacterSkin();
			UpdateCombinedSkin();
		}


		public void ChangeJacketSkin(string skinName)
		{
			selectedJacketSkinName = skinName;
			UpdateCharacterSkin();
			UpdateCombinedSkin();
		}

		public void ChangeWeaponSkin(string skinName)
		{
			selectedWeaponSkinName = skinName;
			UpdateCharacterSkin();
			UpdateCombinedSkin();
		}

		// optimization
		private void RecreateSkin()
		{
			var prevSkin = skeletonAnimation.Skeleton.Skin;
			
			if (runtimeMaterial)
				Destroy(runtimeMaterial);
			if (runtimeAtlas)
				Destroy(runtimeAtlas);
			Skin replacedSkin = prevSkin.GetRepackedSkin("repacked skin", skeletonAnimation.SkeletonDataAsset.atlasAssets[0].PrimaryMaterial, out runtimeMaterial, out runtimeAtlas);
			prevSkin.Clear();

			skeletonAnimation.Skeleton.Skin = replacedSkin;
			skeletonAnimation.Skeleton.SetSlotsToSetupPose();
			skeletonAnimation.AnimationState.Apply(skeletonAnimation.Skeleton);
			
			AtlasUtilities.ClearCache();
			Resources.UnloadUnusedAssets();
		}

		private void UpdateCharacterSkin()
		{
			var skeleton = skeletonAnimation.Skeleton;
			var skeletonData = skeleton.Data;
			characterSkin = new Skin("character-base");
			characterSkin.AddSkin(skeletonData.FindSkin(baseSkin));
			characterSkin.AddSkin(skeletonData.FindSkin(selectedJacketSkinName));
			characterSkin.AddSkin(skeletonData.FindSkin(selectedWeaponSkinName));
		}

		private void UpdateCombinedSkin()
		{
			var skeleton = skeletonAnimation.Skeleton;
			var resultCombinedSkin = new Skin("character-combined");
			
			resultCombinedSkin.AddSkin(characterSkin);
			skeleton.SetSkin(resultCombinedSkin);
			skeleton.SetSlotsToSetupPose();
		}
	}
}
