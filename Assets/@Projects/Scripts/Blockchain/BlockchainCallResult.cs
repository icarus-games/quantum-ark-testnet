using System;

namespace QuantumArk.Blockchain
{
    public struct BlockchainCallResult<T>
    {
        public T result;
        public Exception error;

        public BlockchainCallResult(T result, Exception error)
        {
            this.result = result;
            this.error = error;
        }
    }
}