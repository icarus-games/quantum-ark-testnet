using System;
using UnityEngine;

namespace QuantumArk
{
	public class Movement : MonoBehaviour
	{
		[SerializeField] private float moveSpeed = 5f;
		[SerializeField] private Rigidbody2D rb2d = null;
		[SerializeField] private float moveDamp = 0.1f;

		private int lastDir = 1;

		[Header("Dash")]
		[SerializeField] private float dashSpeed = 20f;

		public Action<Vector3> OnMoving;

		private Vector3 zeroVector = Vector3.zero;

		public void Move(float dir)
		{
			Vector3 targetVelo = new Vector2(dir * moveSpeed, rb2d.velocity.y);
			rb2d.velocity = Vector3.SmoothDamp(rb2d.velocity, targetVelo, ref zeroVector, moveDamp);
			OnMoving?.Invoke(targetVelo);
			if(dir != 0) lastDir = Mathf.CeilToInt(dir);
		}

		public void MoveTowards(Vector3 ownPos, Vector3 target)
		{
			Vector3 movePosition = new Vector3();
			movePosition.x = Mathf.MoveTowards(ownPos.x, target.x, moveSpeed * Time.deltaTime);
			movePosition.y = Mathf.MoveTowards(ownPos.y, target.y, moveSpeed * Time.deltaTime);
			rb2d.MovePosition(movePosition);
		}

		public void MoveTowardsOnAir(Vector3 ownPos, Vector3 target)
		{
			var movePosition = Vector2.MoveTowards(ownPos, target, moveSpeed * Time.deltaTime);
			rb2d.MovePosition(movePosition);
		}

		/// <summary>
		/// Must be -1 or 1
		/// </summary>
		public void ChangeLastDir(int value) => lastDir = value;

		public void ResetVelocity() => rb2d.velocity = new Vector2(0, rb2d.velocity.y);

		public Vector2 CurrentVelocity() => rb2d.velocity;

		public void Dash()
		{
			Vector3 targetVelo = new Vector2(lastDir * dashSpeed, 0);
			rb2d.velocity = targetVelo;
		}
	}
}