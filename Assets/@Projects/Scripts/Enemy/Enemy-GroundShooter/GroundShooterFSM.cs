using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class GroundShooterFSM : EnemyFSM
	{
		public override void Init()
		{
			view.PlayInitate();
			ChangeState(new GroundShooterState_Idle(this));
		}
	}
}
