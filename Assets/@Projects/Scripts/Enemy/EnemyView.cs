using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class EnemyView : MonoBehaviour
	{
		[SerializeField] private SkeletonAnimation skeletonAnimation = null;
		[SerializeField] private string deathVfxName = "enemyDeath";
		[SerializeField] private Transform centerCollider;
		[SerializeField] private float dyingDuration = 0.75f;

		[Header("Shader VFX")]
		[SerializeField] private Animator animator;
		[SerializeField] private string onDamagedTrigger;
		[SerializeField] private string onDyingTrigger;

		private Spine.AnimationState state => skeletonAnimation.AnimationState;

		private const string IDLE_STATE_NAME = "idle";
		private const string RUN_STATE_NAME = "run";
		private const string ATTACK_STATE_NAME = "attack";
		private const string SHOOT_STATE_NAME = "shoot";

		private bool hasPlayDeathAnim = false;

		public void PlayInitate() => AudioPlayer.Instance.PlaySfx("enemyActivated");
		
		public void PlayIdle() => state.SetAnimation(0, IDLE_STATE_NAME, true);

		public void PlayRun() => state.SetAnimation(0, RUN_STATE_NAME, true);

		public void PlayAttack() => state.SetAnimation(0, ATTACK_STATE_NAME, true);

		public void PlayShoot()
		{
			state.SetAnimation(1, ATTACK_STATE_NAME, false);
			AudioPlayer.Instance.PlaySfx("enemyShooting");
		} 

		public void PlayDamaged(Vector3 sourcePos)
		{
			var angle = MathUtility.FindAngleBetweenTwoPoints(sourcePos, centerCollider.position);
			VFXPlayer.Instance.SpawnVFX("enemyDamaged", sourcePos, transform, angle);

			if (!hasPlayDeathAnim) animator.SetTrigger(onDamagedTrigger);
			if(!hasPlayDeathAnim) AudioPlayer.Instance.PlaySfx("enemyDamaged");
		}

		public void PlayStalk()
		{
			StartCoroutine(PlayStalkCo());
		}
		
		private IEnumerator PlayStalkCo()
		{
			while (enabled)
			{
				AudioPlayer.Instance.PlaySfx("stalkerStalk");
				yield return new WaitForSeconds (AudioPlayer.Instance.GetDuration("stalkerStalk"));
			}
		}

		public void PlayDeath()
		{
			if (hasPlayDeathAnim) return;
			hasPlayDeathAnim = true;
			StartCoroutine(PlayDeathCo());
		}

		private IEnumerator PlayDeathCo()
		{
			var prefab = VFXPlayer.Instance.SpawnVFX("enemyDying", centerCollider.position);
			PlayIdle();
			animator.SetTrigger(onDyingTrigger);
			yield return new WaitForSeconds(dyingDuration);
			string sfxName = Random.Range(0, 2) == 0 ? "rocketCollide" : "rocketCollide2";
			AudioPlayer.Instance.PlaySfx(sfxName);
			VFXPlayer.Instance.SpawnVFX(deathVfxName, centerCollider.position);
			CameraFx.Instance.Shake(CameraShakePreset.Medium);
			Lean.Pool.LeanPool.Despawn(prefab);
			Lean.Pool.LeanPool.Despawn(gameObject);
		}

		private void OnEnable()
		{
			hasPlayDeathAnim = false;
		}
	}
}
