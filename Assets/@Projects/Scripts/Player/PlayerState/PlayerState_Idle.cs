﻿using System;
using UniRx;
using UnityEngine;

namespace QuantumArk
{
	public class PlayerState_Idle : BasePlayerState
	{
		public PlayerState_Idle(PlayerFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			input.OnMovePressed().Subscribe(xValue => Move(xValue)).AddTo(disposable);
			input.OnJumpPressed().Subscribe(_ => Jump()).AddTo(disposable);
			input.OnDashPressed().Subscribe(_ => Dash()).AddTo(disposable);
			input.OnShootPressed().Subscribe(isShooting => { Shoot(isShooting); HandleView(isShooting); }).AddTo(disposable);
			Observable.EveryUpdate().Subscribe(_ => CheckOnGrounded()).AddTo(disposable);

			input.OnChangeWeaponPressed().Subscribe(_ => control.SwitchWeapon()).AddTo(disposable);
			input.OnSpecialAttackPressed().Subscribe(_ => SpecialAttack()).AddTo(disposable);
			control.IsShooting.Subscribe(_ => view.PlayIdle(_)).AddTo(disposable);
			control.AimFace.Subscribe(_ => control.ChangeLastDir(_)).AddTo(disposable);
			control.SetOnPlayerDead(ChangeStateToDie);
		}

		private void HandleView(bool isShooting)
		{
			control.IsShooting.Value = isShooting;
		}

		private void Move(float xVal)
		{
			view.UpdateRunDirection(xVal);
			if (xVal != 0f) fsm.ChangeState(new PlayerState_Move(fsm));
			else control.ResetVelocity();
		}

		private void CheckOnGrounded()
		{
			if (!control.IsGrounded())
			{
				fsm.ChangeState(new PlayerState_Jump(fsm));
			}
		}
	}
}