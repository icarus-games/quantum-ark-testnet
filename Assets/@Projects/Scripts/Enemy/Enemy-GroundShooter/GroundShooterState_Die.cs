using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class GroundShooterState_Die : EnemyBaseState
	{
		public GroundShooterState_Die(GroundShooterFSM fsm) : base(fsm)
		{
		}

		public override void OnEnter()
		{
			view.PlayDeath();
		}
	}
}
