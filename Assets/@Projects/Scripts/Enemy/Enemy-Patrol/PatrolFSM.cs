using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumArk
{
	public class PatrolFSM : EnemyFSM
	{
		public override void Init()
		{
			view.PlayInitate();
			ChangeState(new PatrolState_Idle(this));
			ai.InitPatrolPoint();
		}
	}
}
