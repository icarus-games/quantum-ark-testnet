using System;
using QuantumArk.Blockchain;
using UnityEngine;

namespace QuantumArk.UI
{
    public class MainMenuManager : MonoBehaviour
    {
        private void Start()
        {
            Initialize();
        }

        private async void Initialize()
        {
            // if it's connected, try to mint skin A
            var isConnected = BlockchainManager.Instance.IsPlayerConnected;

            if (isConnected)
            {
                LoadingMenu.Open();
                await BlockchainManager.Instance.MintTokenAIfNotExist();
                LoadingMenu.Close();
            }
            
            TitleScreenMenu.Open();
        }
    }
}