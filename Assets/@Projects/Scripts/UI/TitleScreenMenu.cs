using QuantumArk.Blockchain;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace QuantumArk.UI
{
    public class TitleScreenMenu : Menu<TitleScreenMenu>
    {
        [SerializeField] private TitleScreenView view = null;
        
        private void OnEnable()
        {
            view.connectWalletButton.OnClickAsObservable().TakeUntilDisable(this).Subscribe(OnConnectWalletButton);
            view.playButton.OnClickAsObservable().TakeUntilDisable(this).Subscribe(OnPlayButtonClicked);
            view.inventoryButton.OnClickAsObservable().TakeUntilDisable(this).Subscribe(OnInventoryClicked);
            view.logoutButton.OnClickAsObservable().TakeUntilDisable(this).Subscribe(OnLogoutClicked);
            view.quitGameButton.OnClickAsObservable().TakeUntilDisable(this).Subscribe(OnQuitClicked);

            UpdateViewState();                
        }
        
        private async void OnConnectWalletButton(Unit obj)
        {
            LoadingMenu.Open();

            await BlockchainManager.Instance.ConnectPlayer();
            UpdateViewState();                
                
            LoadingMenu.Close();
        }

        private void UpdateViewState()
        {
            var isConnected = BlockchainManager.Instance.IsPlayerConnected;

            view.ChangeState(isConnected);
            if (isConnected) view.SetWalletAddress(BlockchainManager.Instance.AccountId);
        }

        private void OnPlayButtonClicked(Unit obj)
        {
            LoadoutMenu.Open();
        }
        
        private void OnInventoryClicked(Unit obj)
        {
            InventoryMenu.Open();
        }
        
        private async void OnLogoutClicked(Unit obj)
        {
            if (!BlockchainManager.Instance.IsPlayerConnected) return;
            
            LoadingMenu.Open();
            await BlockchainManager.Instance.ConnectPlayer();
            LoadingMenu.Close();
                
            view.ChangeState(BlockchainManager.Instance.IsPlayerConnected);
        }
        
        private void OnQuitClicked(Unit obj)
        {
            Application.Quit();
        }

        public override void OnBackPressed()
        {
        }
    }
}